<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','../');
include(ROOT.'include/common.php');

if(!$session->signed_in){
 header('Location: '.$page->siteRoot.'a/signin');
 exit;
}

$err=array();
if(isset($_POST['submit'])){
 $title=trim(stripslashes(@$_POST['title']));
 $whereto=trim(stripslashes(@$_POST['whereto']));
 $wherefrom=trim(stripslashes(@$_POST['wherefrom']));
 $description=trim(stripslashes(@$_POST['description']));
 
 $date_m=trim(stripslashes(@$_POST['date_m']));
 $date_d=trim(stripslashes(@$_POST['date_d']));
 $date_y=trim(stripslashes(@$_POST['date_y']));
 $time_h=trim(stripslashes(@$_POST['time_h']));
 $time_i=trim(stripslashes(@$_POST['time_i']));
 
 $etime=mktime($time_h,$time_i,0,$date_m,$date_d,$date_y);
 if($etime<time())
  $err[]='The date you have entered is in the past';
 if(empty($title) || empty($whereto) || empty($description))
  $err[]='You need to fill in all of the fields (unless otherwise stated).';

 if(isset($_FILES['poster']) && $_FILES['poster']['size']>0){
  if($_FILES['poster']['error']===0){
   if(!@is_uploaded_file($_FILES['poster']['tmp_name']))
    $err[]='Image upload error: HTTP upload error, try again.';
   elseif(!@getimagesize($_FILES['poster']['tmp_name']))
    $err[]='Image upload error: the file must be an image!';
  }else{
   $img_errs=array(
    1=>'php.ini max file size exceeded',
    2=>'html form max file size exceeded',
    3=>'file upload was only partial',
    4=>'no file was attached');
   $err[]='Image upload error: '.$img_errs[$_FILES['poster']['error']];
	}
 }
	
 if(!count($err)){
	$id=$data_events[count($data_events)-1]['id']+1;
  $time=time();

  if(isset($_FILES['poster']) && $_FILES['poster']['size']>0){
	 $ext=pathinfo($_FILES['poster']['name']);
   $ext=$ext['extension'];
	 $poster="$id-$time.$ext";
   @move_uploaded_file($_FILES['poster']['tmp_name'],ROOT.'images/events/'.$poster);
	}else
	 $poster=''; 
  
	$data_events[]=array(
   'id'=>$id,
   'datetime'=>$time,
   'author'=>$pun_user['username'],
   'date'=>date('d/m/Y',$etime),
   'time'=>date('H:i',$etime),
   'title'=>$title,
   'whereto'=>$whereto,
   'wherefrom'=>$wherefrom,
   'description'=>$description,
   'poster'=>$poster
	);
  recache_arr('$data_events',$data_events,ROOT.'data/events.php');
  header('Location: '.$page->siteRoot.'events');
  exit;
 }
}else{
 $date_d=date('j',mktime(0,0,0,date('m'),date('d'),date('Y')));
 $date_m=date('n',mktime(0,0,0,date('m'),date('d'),date('Y')));
 $date_y=date('Y',mktime(0,0,0,date('m'),date('d'),date('Y')));
}

$page->title='Administration &#187; Add Event';
$page->pagetitle='Add an Event to the CGCU website';
$page->head();
?>
    <h3>Add an Event</h3>
<?php if(count($err)){?>
    <ul class="error"><li><?php echo implode('</li><li>',$err);?></li></ul>
<?php }?>
    <form action="<?php echo $page->siteRoot.'a/addevent';?>" method="post" class="post" enctype="multipart/form-data">
     What is the event:<br />
     <input type="text" name="title" value="<?php echo htmlspecialchars($title);?>" class="text" /><br />
     Poster (not required):<br />
     <input type="file" name="poster" value="" class="file" /><br />
     When will it be:<br />
     <select name="date_d" style="width:60px;"><?php for($d=1;$d<32;$d++){echo "<option value=\"$d\"".(date('j',mktime(0,0,0,$date_m,$date_d,$date_y))==$d&&$date_d!=''?' selected="selected"':'').">$d</option>";}?></select> 
     <select name="date_m" style="width:110px;"><?php for($d=1;$d<13;$d++){echo "<option value=\"$d\"".(date('n',mktime(0,0,0,$date_m,$date_d,$date_y))==$d&&$date_m!=''?' selected="selected"':'').">".date('F',mktime(0,0,0,$d,1,0))."</option>";}?></select>
     <select name="date_y" style="width:80px;margin-right:20px;"><?php for($d=date('Y');$d<date('Y')+2;$d++){echo "<option value=\"$d\"".(date('Y',mktime(0,0,0,$date_m,$date_d,$date_y))==$d&&$date_y!=''?' selected="selected"':'').">$d</option>";}?></select>
     <select name="time_h" style="width:50px;"><?php for($th=0;$th<24;$th++){echo '<option value="'.$th.'"'.($time_h==$th||(!isset($time_h)&&$th==18)?' selected="selected"':'').'>'.str_pad($th,2,'0',STR_PAD_LEFT).'</option>';}?></select>
     <select name="time_i" style="width:50px;"><?php for($ti=0;$ti<60;$ti+=5){echo '<option value="'.$ti.'"'.($time_i==$ti?' selected="selected"':'').'>'.str_pad($ti,2,'0',STR_PAD_LEFT).'</option>';}?></select><br />
     Where will it take place:<br />
     <input type="text" name="whereto" value="<?php echo htmlspecialchars($whereto);?>" class="text" /><br />
     Meet anywhere beforehand (not required):<br />
     <input type="text" name="wherefrom" value="<?php echo htmlspecialchars($wherefrom);?>" class="text" /><br />
     Description:<br />
     <div id="toolbar"></div>
     <script language="JavaScript" type="text/javascript">
      <!--
      <?php 
      for($i=0;$i<count($tools);$i++){
       echo "format.tools[{$i}]=[];";
			 foreach($tools[$i] as $k=>$t) echo "format.tools[{$i}]['{$k}']='{$t}';";
			}?> 
      format.load('<?php echo $page->siteRoot;?>images/toolbar/','toolbar','description');
      //-->
     </script>
     <textarea id="description" name="description" rows="5" cols="40" class="text"><?php echo htmlspecialchars($description);?></textarea>
     <p class="center">
      <input type="submit" name="submit" value="Save" class="button" />
      <input type="button" value="Cancel" class="button" onclick="window.location='<?php echo $page->siteRoot;?>a/'" />
     </p>
    </form>
<?php  
toolbar_popups();
$page->foot();
?>