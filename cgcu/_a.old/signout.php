<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','../');
include(ROOT.'include/common.php');

$session->signout();

if(isset($_GET['r']) && !empty($_GET['r'])){
 header('Location: '.$_GET['r']);
}else{
 header('Location: '.$page->siteRoot);
}
?>