<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','../');
include(ROOT.'include/common.php');

if(!$session->signed_in){
 header('Location: '.$page->siteRoot.'a/signin');
 exit;
}

if(isset($_GET['deleteall'])){
 if($_GET['deleteall']==$_SESSION['delete_code'] && !empty($_SESSION['delete_code']))
  recache_arr('$data_events',array(),ROOT.'data/events.php');
 header('Location: '.$page->siteRoot.'a/editevent');
 exit;
}
$_SESSION['delete_code']=code(50);

$err=array();
if(isset($_POST['save'])){
 $index=find_id($data_events,(int)$_GET['p1']);
 if($index>-1){
  $title=trim(stripslashes(@$_POST['title']));
  $whereto=trim(stripslashes(@$_POST['whereto']));
  $wherefrom=trim(stripslashes(@$_POST['wherefrom']));
  $description=trim(stripslashes(@$_POST['description']));
 
  $date_m=trim(stripslashes(@$_POST['date_m']));
  $date_d=trim(stripslashes(@$_POST['date_d']));
  $date_y=trim(stripslashes(@$_POST['date_y']));
  $time_h=trim(stripslashes(@$_POST['time_h']));
  $time_i=trim(stripslashes(@$_POST['time_i']));
 
  $etime=mktime($time_h,$time_i,0,$date_m,$date_d,$date_y);
	
  if(empty($title) || empty($whereto) || empty($description))
   $err[]='You need to fill in all of the fields (unless otherwise stated).';

  if(isset($_FILES['poster']) && $_FILES['poster']['size']>0){
   if($_FILES['poster']['error']===0){
    if(!@is_uploaded_file($_FILES['poster']['tmp_name']))
     $err[]='Image upload error: HTTP upload error, try again.';
    elseif(!@getimagesize($_FILES['poster']['tmp_name']))
     $err[]='Image upload error: the file must be an image!';
   }else{
    $img_errs=array(
     1=>'php.ini max file size exceeded',
     2=>'html form max file size exceeded',
     3=>'file upload was only partial',
     4=>'no file was attached');
    $err[]='Image upload error: '.$img_errs[$_FILES['poster']['error']];
   }
  }
	 
  if(!count($err)){
   if(isset($_FILES['poster']) && $_FILES['poster']['size']>0){
    $ext=pathinfo($_FILES['poster']['name']);
    $ext=$ext['extension'];
    $poster=$_GET['e'].'-'.time().".$ext";
    @move_uploaded_file($_FILES['poster']['tmp_name'],ROOT.'images/events/'.$poster);
   }elseif(isset($_POST['noposter']))
	  $poster='';
	 else
    $poster=$data_events[$index]['poster']; 
	 
   $data_events[$index]['date']=date('d/m/Y',$etime);
   $data_events[$index]['time']=date('H:i',$etime);
   $data_events[$index]['title']=$title;
   $data_events[$index]['whereto']=$whereto;
   $data_events[$index]['wherefrom']=$wherefrom;
	 $data_events[$index]['description']=$description;
   $data_events[$index]['poster']=$poster;

   recache_arr('$data_events',$data_events,ROOT.'data/events.php');
   header('Location: '.$page->siteRoot.'events');
   exit;
  }
 }else{
  $err[]='noexist';
 }
}
if(isset($_POST['delete'])){
 $index=find_id($data_events,(int)$_GET['p1']);
 if($index>-1){
  unset($data_events[$index]);
  recache_arr('$data_events',$data_events,ROOT.'data/events.php');
  header('Location: '.$page->siteRoot.'events');
  exit;
 }else{
  $err='noexist';
 }
}

$page->title='Administration &#187; Edit and Event';
$page->pagetitle='Edit an Event on the CGCU website';
$page->head();

if(isset($_GET['p1'])){
 $index=find_id($data_events,(int)$_GET['p1']);
 if($index>-1){
  if(!isset($_POST['submit'])){
	 $title=$data_events[$index]['title'];
	 $whereto=$data_events[$index]['whereto'];
	 $wherefrom=$data_events[$index]['wherefrom'];
	 $description=$data_events[$index]['description'];
	 $poster=$data_events[$index]['poster'];

	 list($date_d,$date_m,$date_y)=explode('/',$data_events[$index]['date']);
	 list($time_h,$time_i)=explode(':',$data_events[$index]['time']);
  }
?>
    <h3>Edit an Event</h3>
<?php if(count($err)){?>
    <ul class="error"><li><?php echo implode('</li><li>',$err);?></li></ul>
<?php }?>
    <form action="<?php echo $page->siteRoot.'a/editevent/'.$_GET['p1'];?>" method="post" class="post" enctype="multipart/form-data">
     What is the event:<br />
     <input type="text" name="title" value="<?php echo $title;?>" class="text" /><br />
<?php 
if($poster!=''){
?>
     Current poster:<br />
     <img src="<?php echo $page->siteRoot;?>images/image.php?size=h80&amp;file=events/<?php echo $poster;?>" alt="The current poster for this event" /><br />
     Change poster (not required):<br />
     <input type="file" name="poster" value="" class="file" /><br />
     or <label for="noposter">remove the current poster: </label><input type="checkbox" id="noposter" name="noposter" /><br /><br />
<?php 
}else{
?>
     Add a Poster (not required):<br />
     <input type="file" name="poster" value="" class="file" /><br />
<?php 
}
?>
     When will it be:<br />
     <select name="date_d" style="width:60px;"><?php for($d=1;$d<32;$d++){echo "<option value=\"$d\"".(date('j',mktime(0,0,0,$date_m,$date_d,$date_y))==$d&&$date_d!=''?' selected="selected"':'').">$d</option>";}?></select> 
     <select name="date_m" style="width:110px;"><?php for($d=1;$d<13;$d++){echo "<option value=\"$d\"".(date('n',mktime(0,0,0,$date_m,$date_d,$date_y))==$d&&$date_m!=''?' selected="selected"':'').">".date('F',mktime(0,0,0,$d,1,0))."</option>";}?></select>
     <select name="date_y" style="width:80px;margin-right:20px;"><?php for($d=date('Y');$d<date('Y')+2;$d++){echo "<option value=\"$d\"".(date('Y',mktime(0,0,0,$date_m,$date_d,$date_y))==$d&&$date_y!=''?' selected="selected"':'').">$d</option>";}?></select>
     <select name="time_h" style="width:50px;"><?php for($th=0;$th<24;$th++){echo '<option value="'.$th.'"'.($time_h==$th||(!isset($time_h)&&$th==18)?' selected="selected"':'').'>'.str_pad($th,2,'0',STR_PAD_LEFT).'</option>';}?></select>
     <select name="time_i" style="width:50px;"><?php for($ti=0;$ti<60;$ti+=5){echo '<option value="'.$ti.'"'.($time_i==$ti?' selected="selected"':'').'>'.str_pad($ti,2,'0',STR_PAD_LEFT).'</option>';}?></select><br />
     Where will it take place:<br />
     <input type="text" name="whereto" value="<?php echo $whereto;?>" class="text" /><br />
     Meet anywhere beforehand (not required):<br />
     <input type="text" name="wherefrom" value="<?php echo $wherefrom;?>" class="text" /><br />
     Description:<br />
     <div id="toolbar"></div>
     <script language="JavaScript" type="text/javascript">
      <!--
      <?php 
      for($i=0;$i<count($tools);$i++){
       echo "format.tools[{$i}]=[];";
			 foreach($tools[$i] as $k=>$t) echo "format.tools[{$i}]['{$k}']='{$t}';";
			}?> 
      format.load('<?php echo $page->siteRoot;?>images/toolbar/','toolbar','description');
      //-->
     </script>
     <textarea id="description" name="description" rows="5" cols="40" class="text"><?php echo $description;?></textarea>
     <p class="center">This event was added on <strong><?php echo date('d/m/Y H:i:s',(int)$data_events[$index]['datetime']);?></strong> by <strong><?php echo $data_events[$index]['author'];?></strong>.</p>
     <p class="center">
      <input type="submit" name="delete" value="Delete" class="button" onclick="return confirm('Are you sure you want to delete this event?');" />
      <input type="submit" name="save" value="Edit" class="button" />
      <input type="button" value="Cancel" class="button" onclick="window.location='<?php echo $page->siteRoot;?>a/editevent'" />
     </p>
    </form>
<?php  
  toolbar_popups(); 
 }else{
?>
    <h3>Event cannot be found</h3>
    <ul class="error"><li>Sorry but the event you want to edit just isn't there.</li></ul>
<?php 
 }
}else{
?>
    <h3>Choose an Event to Edit</h3>
<?php  
 if(count($data_events)>0){
?>
    <ul><?php 
foreach($data_events as $event){
 ?><li><a href="<?php echo $page->siteRoot.'a/editevent/'.$event['id'];?>"><?php echo $event['title'];?></a> - <?php echo $event['date'];?></li><?php 
}
?></ul>
    <p style="margin-top:20px;"><a href="#" onclick="if(confirm('Are you sure you want to delete ALL of the events?')){window.location='<?php echo $page->siteRoot;?>a/editevent?deleteall=<?php echo $_SESSION['delete_code'];?>';}return false;" class="edit">DELETE ALL EVENTS</a></p>
<?php  
 }else{?>
    <ul class="error"><li>There are no events, BORING.</li></ul>
<?php }
}
$page->foot();
?>