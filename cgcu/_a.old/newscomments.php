<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','../');
include(ROOT.'include/common.php');

if(!$session->signed_in){
 header('Location: '.$page->siteRoot.'a/signin');
 exit;
}

if(isset($_GET['filename']) && isset($_GET['comment'])){
 $filename=$_GET['filename'];
 $comment=(int)$_GET['comment'];
 if($contents=@file_get_contents(ROOT.'data/news/'.$filename)){
  if($_GET['action']=='accept'){
   $contents=preg_replace('/<!--comment_start--><!--comment_allowed\[0\]--><!--comment_id\['.$comment.'\]-->(.*?)<!--comment_end-->/',"<!--comment_start--><!--comment_allowed[1]--><!--comment_id[$comment]-->$1<!--comment_end-->",$contents);
	}elseif($_GET['action']=='delete'){
   $contents=preg_replace("/(?:\r)?\n <!--comment_start--><!--comment_allowed\[(0|1)\]--><!--comment_id\[{$comment}\]-->(.*?)<!--comment_end-->/s",'',$contents);
	}
  $fp=fopen(ROOT.'data/news/'.$filename,'w');
  @flock($fp,LOCK_EX);
  fwrite($fp,$contents);
  @flock($fp,LOCK_UN);
  fclose($fp);
 }
 header('Location: '.$page->siteRoot.'a/newscomments');
 exit;
}

$page->title='Administration &#187; Moderate';
$page->pagetitle='Moderate News Item Comments';
$page->head();

?>
    <h3>News Item Comments Awaiting Moderation</h3>
<?php 
 $handle=opendir(ROOT.'data/news/');
 while(false!==($file=readdir($handle))){
  if($file!="." && $file!=".."){
   if(preg_match("/^\d{5}\.php$/",$file)) $files[]=$file;
  }
 }
 closedir($handle);
 if(isset($files)){
  rsort($files);
?>
    <ul><?php 
  foreach($files as $file){
   if($contents=@file_get_contents(ROOT.'data/news/'.$file)){
    preg_match('/<!--title\[(.*?)\]-->/',$contents,$title);
    preg_match_all('/<!--comment_start--><!--comment_allowed\[0\]-->(.*?)<!--comment_end-->/s',$contents,$comments);
		if(count($comments[1])>0){
		 $mod=true;
		 foreach($comments[1] as $comment){
      preg_match('/<!--comment_id\[(.*?)\]-->/',$comment,$id);
      preg_match('/<!--comment_name\[(.*?)\]-->/',$comment,$name);
      preg_match('/<!--comment_email\[(.*?)\]-->/',$comment,$email);
      preg_match('/<!--comment_comment\[(.*?)\]-->/',$comment,$message);
      ?><li>Post: <strong><a href="<?php echo $page->siteRoot;?>archives/<?php echo str_replace('.php','',$file);?>"><?php echo $title[1];?></strong></a><br />Name: <strong><?php echo $name[1];?></strong><br />Email: <strong><?php echo $email[1];?></strong><br />Comment: <strong><?php echo $message[1];?></strong><br /><?php 
			?><p style="text-align:center;margin:5px 0;padding:5px;background:#f5f5f5;border-bottom:2px solid #eaeaea;"><a href="<?php echo $page->siteRoot;?>a/newscomments?filename=<?php echo $file;?>&amp;comment=<?php echo $id[1];?>&amp;action=accept" class="image"><img src="<?php echo $page->siteRoot;?>images/accept.png" alt="Accept" /></a> or <?php 
			?><a href="<?php echo $page->siteRoot;?>a/newscomments?filename=<?php echo $file;?>&amp;comment=<?php echo $id[1];?>&amp;action=delete" class="image"><img src="<?php echo $page->siteRoot;?>images/delete.png" alt="Delete" /></a><?php 
			?></p></li><?php
		 }
		} 
   }
  }
	if(!isset($mod)){
   ?><li>There are no new comments to moderate.</li><?php
	} 
  ?></ul><?php 
 }else{
?>
    <ul class="error"><li>There is no society news, how boring!</li></ul>
<?php 
 }
$page->foot();
?>