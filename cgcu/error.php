<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ERROR_PAGE',true);
define('ROOT','./');
include(ROOT.'include/common.php');

$page->title='Oops! Page Not Found...';
$page->pagetitle='Oops! Page Not Found...';

$page->head();
?>
    <h3>Oh no! What's all this then?</h3>
    <ul class="error"><li>Well we've looked everywhere, through all the directory's and even the trash but the page you're looking for just isn't there.. This could be due to one of a variey of reasons:<ul><li><em>You decided to randomly type in a url to see where it would take you..</em></li><li><em>You were directed here by another website such as a search engine</em></li></ul></li><li>You can try the <a href="<?php echo $page->siteRoot;?>">homepage</a> or try <a href="http://www.google.com/">searching</a> for what you're looking for or..</li><li>If you definately think the error is on our part then please submit a <a href="<?php echo $page->siteRoot;?>contact/?subject=Bug%20Report">bug report</a>.</li></ul>
    <div class="hr"></div>
    <h3>Considering you're already here...</h3>
    <p>...why not check out some of the site content, have a look at:</p>
    <ul><li>The <a href="<?php echo $page->siteRoot;?>archives">archives</a> page to read up on the latest CGCU news;</li><li>The <a href="<?php echo $page->siteRoot;?>events">events</a> page to see what great events the CGCU has organised;</li><li>Or check out the <a href="<?php echo $page->siteRoot;?>clubs-and-societies">Clubs &amp; Societies</a>.</li></ul>
<?php 
$page->foot();
?>