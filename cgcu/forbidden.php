<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','./');
include(ROOT.'include/common.php');

$page->title='Oops! Sorry, private page';
$page->pagetitle='Oops! Sorry, private page';

$page->head();
?>
    <h3>Oh no! What's all this then?</h3>
    <ul class="error"><li>Well it turns out that the page or directory you want to view is kinda private and you don't have the necessary permission to view it!</li></ul>
    <div class="hr"></div>
    <h3>Considering you're already here...</h3>
    <p>...why not check out some of the site content, have a look at:</p>
    <ul><li>The <a href="<?php echo $page->siteRoot;?>archives">archives</a> page to read up on the latest CGCU news;</li><li>The <a href="<?php echo $page->siteRoot;?>events">events</a> page to see what great events the CGCU has organised;</li><li>Or check out the <a href="<?php echo $page->siteRoot;?>clubs-and-societies">Clubs &amp; Societies</a>.</li></ul>
<?php 
$page->foot();
?>