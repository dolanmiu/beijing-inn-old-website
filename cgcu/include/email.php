<?php 
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/
if(!defined('BEGIN')){
 die('Die: Hacking attempt');
}

function is_valid_email($email){
 if(strlen($email)>50) return false;
 return preg_match('/^(([^<>()[\]\\.,;:\s@"\']+(\.[^<>()[\]\\.,;:\s@"\']+)*)|("[^"\']+"))@((\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])|(([a-zA-Z\d\-]+\.)+[a-zA-Z]{2,}))$/',$email);
}
function linebreaks($str){
 return str_replace("\r","\n",str_replace("\r\n","\n",$str));
}
function send_mail($to,$subject,$message,$from=''){
 global $page;

 if(!$from)
  $from='"'.str_replace('"','',$page->tagline.' Mailer').'" <guilds@cgcu.net>';

 $to=trim(preg_replace('#[\n\r]+#s','',$to));
 $subject=trim(preg_replace('#[\n\r]+#s','',$subject));
 $from=trim(preg_replace('#[\n\r:]+#s','',$from));
 $headers='From: '.$from."\r\n".'Date: '.date('r')."\r\n".'MIME-Version: 1.0'."\r\n".'Content-type: text/html; charset=iso-8859-1'."\r\n".'X-Mailer: CGCU Mailer';
 $message=str_replace(array("\n","\0"),array("\r\n",''),linebreaks($message));

 if(strtoupper(substr(PHP_OS,0,3))=='MAC')
  $headers=str_replace("\r\n","\r",$headers);
 elseif(strtoupper(substr(PHP_OS,0,3))!='WIN')
  $headers=str_replace("\r\n","\n",$headers);

 return @mail($to,$subject,$message,$headers);
}
?>
