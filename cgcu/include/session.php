<?php 
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/  
if(!defined('BEGIN')){
 die('Die: Hacking attempt');
}

Class Session{
 var $username;
 var $userinfo=array();
 var $signed_in=false;

 function Session(){
  global $global;
  include(ROOT.'include/userinfo.php');

  if(isset($_COOKIE['user']) && isset($_COOKIE['ssid'])){
   $_SESSION['user']=$_COOKIE['user'];
   $_SESSION['ssid']=$_COOKIE['ssid'];
  }
  if(isset($_SESSION['user']) && isset($_SESSION['ssid'])){
   $this->userk=-1;
   foreach($_userinfo as $k=>$ui){
    if($ui['user']==$_SESSION['user'] && $ui['ssid']==$_SESSION['ssid'])
     $this->userk=$k;
   }
   if($this->userk<0){
    unset($_SESSION['user'],$_SESSION['ssid']);
    setcookie('ssid','',time()-$global['cookie_expire'],$global['cookie_path']);
   }
   $this->userinfo=$_userinfo[$this->userk];
   $this->username=$_SESSION['user']=$this->userinfo['user'];
   $this->userinfo['time_active']=time();

   if(strpos($_SERVER['PHP_SELF'],'.php')!==false)
    $this->userinfo['url']=$_SERVER['PHP_SELF'];

   $nui=$_userinfo;
	 $nui[$this->userk]=$this->userinfo;
	 recache_arr('$_userinfo',$nui,ROOT.'include/userinfo.php');
	 
   $this->signed_in=true;
  }
	
  include(ROOT.'data/stats.php');
  $stats['online'][$this->signed_in?$this->username:$_SERVER['REMOTE_ADDR']]=
   array('time'=>time(),'url'=>"{$_SERVER['REQUEST_URI']} ({$_SERVER['PHP_SELF']})");
	foreach($stats['online'] as $k=>$v)
   if(time()>$v['time']+(5*60))
    unset($stats['online'][$k]);
	if(count($stats['online'])>$stats['most_online'])
   $stats['most_online']=count($stats['online']);
	$this->stats=$stats;
  recache_arr('$stats',$stats,ROOT.'data/stats.php');
 }

 function signin($user,$pass,&$error){
  global $global;
  include(ROOT.'include/userinfo.php');

  if(!$user || strlen($user=stripslashes(trim($user)))==0) $error['user']='You must fill out the <strong><label for="user">username</label></strong> field.<br />';
  if(!$pass || strlen($pass=stripslashes(trim($pass)))==0) $error['pass']='You must fill out the <strong><label for="pass">password</label></strong> field.<br />';
  if(count($error)>0) return false;

	$this->userk=-1;
	foreach($_userinfo as $k=>$ui){
	 if($ui['user']==$user && $ui['pass']==md5($pass))
    $this->userk=$k;
	}
  if($this->userk<0)
   $error['pass']='Your login details were <strong>incorrect</strong>. Please try again<br />';
  if(count($error)>0) return false;

  $this->userinfo=$_userinfo[$this->userk];
  $this->username=$_SESSION['user']=$this->userinfo['user'];
  $_SESSION['ssid']=$this->userinfo['ssid']=$this->generateRandID();
	$this->userinfo['time_signin']=time();
	$this->userinfo['time_active']=time();

  $_userinfo[$this->userk]=$this->userinfo;
	recache_arr('$_userinfo',$_userinfo,ROOT.'include/userinfo.php');

  setcookie('user',$this->username,time()+$global['cookie_expire'],$global['cookie_path']);
	
  include(ROOT.'data/stats.php');
  unset($stats['online'][$_SERVER['REMOTE_ADDR']]);
	$this->stats=$stats;
  recache_arr('$stats',$stats,ROOT.'data/stats.php');

  $this->signed_in=true;
  return true;
 }

 function signout(){
  global $global;

  unset($_SESSION['user'],$_SESSION['ssid']);
  setcookie('user','',time()-$global['cookie_expire'],$global['cookie_path']);
  setcookie('ssid','',time()-$global['cookie_expire'],$global['cookie_path']);
	
  include(ROOT.'data/stats.php');
  unset($stats['online'][$this->username]);
	$this->stats=$stats;
  recache_arr('$stats',$stats,ROOT.'data/stats.php');

  $this->signed_in=false;
 }

 function is_admin(){
  return in_array($this->username,array('admin','mjphaynes','awb01','foepres'));
 }

 function generateRandID(){return md5($this->generateRandStr(16));}
 function generateRandStr($length){
  for($i=0;$i<$length;$i++){$randnum=mt_rand(0,61);if($randnum<10){$randstr.=chr($randnum+48);}else if($randnum<36){$randstr.=chr($randnum+55);}else{$randstr.=chr($randnum+61);}}
  return $randstr;
 }
};
$session=new Session;
?>
