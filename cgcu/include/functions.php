<?php 
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/
if(!defined('BEGIN')){
 die('Die: Hacking attempt');
}
if(!function_exists('curl_setopt_array')){
 function curl_setopt_array(&$ch,$curl_options){
  foreach($curl_options as $option=>$value)
   if(!curl_setopt($ch,$option,$value))
    return false;
  return true;
 }
}
function fetch_page($url){
 $options=array(
  CURLOPT_RETURNTRANSFER=>true,  # return web page
  CURLOPT_NOBODY        =>false,
  CURLOPT_FOLLOWLOCATION=>true,  # follow redirects
  CURLOPT_AUTOREFERER   =>true,  # set referer on redirect
  CURLOPT_CONNECTTIMEOUT=>120,   # timeout on connect
  CURLOPT_TIMEOUT       =>120,   # timeout on response
  CURLOPT_MAXREDIRS     =>10     # stop after 10 redirects
 );
 if($page->siteRoot=='/')
  $options[CURLOPT_PROXY]="wwwcache.imperial.ac.uk:3128";
 $ch=curl_init($url);
 curl_setopt_array($ch,$options);
 if(($content=curl_exec($ch))===false) 
  return '';
 curl_close($ch);
 return (string)$content;
}
function read_url($url){
 global $page;
 if($page->siteRoot=='/'){
  $proxy='wwwcache.imperial.ac.uk';
  $port=3128;
 }
 $content = '';
 if(empty($proxy)){
  $fp=fopen($url,'r');
  if(!$fp) return false;
  while(!feof($fp)) $content=$content.fread($fp,4096);
  fclose($fp);
 }else{
  $fp=fsockopen($proxy,$port);
  if(!$fp)
   return false;

  fputs($fp,"GET $url HTTP/1.0\r\nHost: $proxy\r\n\r\n");
  while(!feof($fp)) $content=$content.fread($fp,4096);
  fclose($fp);
  $content=substr($content,strpos($content,"\r\n\r\n")+4);
 }
 return $content;
} 
function url_maker($title,$date=''){
 return preg_replace("/[^-a-z0-9]/",'',str_replace(array(' ','/'),array('-','-'),strtolower((!empty($date)?$date.'-':'').$title)));
}
function strip_crap($str){
 $str=str_replace('�','',$str);
 $str=preg_replace("#[^\s\d\w<>,\.\\/;:'@\#~\[\]{}\?\!\"*-_\+=]#",'',$str);
 return $str;
}
function word_limit($str,$limit=100,$end_char='&#8230;'){
 if(trim($str)=='') return $str;
 preg_match('/^\s*(?:\S+\s*){1,'.(int)$limit.'}/',$str,$matches);
 if(strlen($matches[0])==strlen($str)) $end_char='';
 return rtrim($matches[0]).$end_char;
}
function parse_xml($xml){
 return $arr;
}
function recache_arr($arr_name,$arr,$fname){
 $c="<"."?php\n/*\n * @project:  CGCU Website\n * @author:   Michael Haynes\n * @contact:  mjh105@imperial.ac.uk\n * @date:     May 2008\n *\n * Please do not change anything\n * unless you know what you're doing!\n *\n*/\nif(!defined('BEGIN')){\n die('Die: Hacking attempt');\n}\n\n";
 $c.="$arr_name=".var_export($arr,true).';';
 $c.="\n\n?".">";
 $fh=fopen($fname,'w');
 fwrite($fh,$c);
 fclose($fh);
}
function char_limit($str,$limit=45,$end_char='&#8230;'){
 if(strlen($str)>$limit-1){
  return substr($str,0,$limit-1).$end_char;
 }else return $str;
}
function field($value){
 if(strpos($value,'&quot;')!==false ||
    strpos($value,'&#039;')!==false ||
    strpos($value,'&amp;')!==false
	)
  return stripslashes($value);
 else return htmlspecialchars(stripslashes($value),ENT_QUOTES);
}
function obfuscate($istr){
 $ostr='';
 for($i=0;$i<strlen($istr);$i++){
  if(mt_rand(0,1))
   $ostr.=substr($istr,$i,1);
  elseif(mt_rand(0,1))
   $ostr.='&#'.ord(substr($istr,$i,1)).';';
  else
   $ostr.='&#x'.sprintf("%x",ord(substr($istr,$i,1))).';';
 }
 $ostr=str_replace(array('<'),array('&lt;'),$ostr);
 if(mt_rand(0,1)) return str_replace('@','&#64;',$ostr);
 else return str_replace('@','&#x40;',$ostr);
}

function parse_html($buffer){
 global $global,$page,$data_clubs;
 # $buffer=preg_replace("#<ul>[\r\n\s]+<li#",'<ul><li',$buffer);
 # $buffer=preg_replace("#</li>[\r\n\s]+<li#",'</li><li',$buffer);
 # $buffer=preg_replace("#</li>[\r\n\s]+</ul>#",'</li></ul>',$buffer);
 if(!isset($global['maintenance']) || $global['maintenance']){
  $buffer=preg_replace("#/\* Page specific styling \*/#","/* Page specific styling */\n    div#content{\n     width:760px;\n    }\n    div#content div#crufts{\n     margin-right:0;\n    }",$buffer);
  if(count($data_clubs)>0){
	 $cstr="<li>If you are trying to find a club website then they can be found at the following links:<ul>";
	 foreach($data_clubs['deps'] as $c=>$u) $cstr.="<li><strong>$c</strong> - <a href=\"{$u['site']}\">{$u['site']}</a></li>";
	 $cstr.="</ul></li>";
	}
  $mstr="    <h3>Site Undergoing Maintenance</h3>\n    <ul><li>The site administrator has turned off the pages of the site so that essential maintenance work can be done.<br />Please check back soon!</li>$cstr<li>If you need to contact the CGCU you can do so with the following email address: ".$page->mail_link('guilds@cgcu.net')."</li></ul>";
  $mstr="\n   <div id=\"content\">\n    <div id=\"crufts\">\n     <ul><li id=\"cruft-home\"><a href=\"/cgcu/\">Home</a></li><li id=\"cruft-maintenance\">Maintenance</li></ul>\n     <h2>Site Offline for Maintenance</h2>\n     <br style=\"clear:both;\" />\n    </div>\n    <div id=\"topcontact\">\n     <a id=\"contact\" href=\"{$page->siteRoot}contact\">Contact</a>\n     <br style=\"clear:both;\" />\n    </div>\n$mstr\n   </div>\n   <br style=\"clear:both;\" />";
  $buffer=preg_replace("#<div id=\"container\">(.*?)<div id=\"footer\">#is","<div id=\"container\">$mstr\n  </div>\n  <div id=\"footer\">",$buffer);
 }
 return $buffer;
}
function format_arr($n,$arr){
 $tmp=array();
 foreach($arr as $k=>$v){
  if(is_array($v)){
	 $tmp2=array();
	 foreach($v as $k2=>$v2)
    $tmp2[]="'$k2'=>'".slash($v2)."'";
   $tmp[]="array(".implode(",",$tmp2).")";
	}else
	 $tmp[]="'$k'=>'".slash($v)."'";
 }
 return "<"."?php \n\n\${$n}=array(\n ".implode(",\n ",$tmp)."\n);\n\n?".">";
}
function slash($str){
 if(strpos($str,"'")!==false)
  return htmlentities($str,ENT_QUOTES);
 else
  return $str;
}
function find_id($arr,$id){
 foreach($arr as $k=>$v)
  if($v['id']==$id)
	 return $k;
 return -1;
}
function find_pos($arr,$pos){
 for($i=0;$i<count($arr);$i++)
  if(strtolower($arr[$i]['position'])==strtolower($pos))
	 return $i;
 return -1;
}
function code($len=16){
 $code='';
 $salt='abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789';
 srand((double)microtime()*1000000);
 for($i=0;$i<$len;$i++)
  $code.=strtoupper(substr($salt,rand()%strlen($salt),1));
 return $code;
}
$tools=array(
 array('text'=>'Bold','image'=>'text_bold.png','fn'=>'bold'),
 array('text'=>'Italic','image'=>'text_italic.png','fn'=>'italic'),
 array('text'=>'Underline','image'=>'text_underline.png','fn'=>'underline'),
 array('text'=>'Strike through','image'=>'text_strikethrough.png','fn'=>'strike'),
 array('text'=>'Align left','image'=>'text_align_left.png','fn'=>'align','option'=>'left'),
 array('text'=>'Align center','image'=>'text_align_center.png','fn'=>'align','option'=>'center'),
 array('text'=>'Align right','image'=>'text_align_right.png','fn'=>'align','option'=>'right'),
 array('text'=>'Align justify','image'=>'text_align_justify.png','fn'=>'align','option'=>'justify'),
 array('text'=>'Unordered list','image'=>'text_list_bullets.png','fn'=>'ul'),
 array('text'=>'Ordered list','image'=>'text_list_numbers.png','fn'=>'ol'),
 array('text'=>'Superscript','image'=>'text_superscript.png','fn'=>'sup'),
 array('text'=>'Subscript','image'=>'text_subscript.png','fn'=>'sub'),
 array('text'=>'Add link','image'=>'link.png','fn'=>'url','option'=>'link here'),
 array('text'=>'Add image','image'=>'image.png','fn'=>'img')
);
function toolbar_popups(){
 global $page,$site_links;
?>
    <div id="uridiv" style="position:absolute;top:-9999px;left:-9999px;">
     <h3>Add a link</h3>
     <form action="<?php echo $page->siteRoot;?>a/" method="post" class="post" onsubmit="format.url(this);return false;">
      Enter text to display:<br />
      <input type="text" id="uri_text" name="uri_text" value="<?php echo htmlspecialchars($uri_text);?>" class="text" /><br />
      Enter URI:<br />
      <input type="text" name="uri_uri" value="<?php echo htmlspecialchars($uri_uri);?>" class="text" /><br />
      <strong>OR</strong> choose page on site:<br />
      <select name="uri_select">
<?php 
foreach($site_links as $link=>$text)
 echo "       <option value=\"{$page->siteRoot}{$link}\">&nbsp;{$text}</option>\n";
?>
      </select>
      <p class="center">
       <input type="submit" name="save" value="Add" class="button" />
       <input type="button" value="Cancel" class="button" onclick="tb_remove();" />
      </p>
     </form>
    </div>
    <div id="imgdiv" style="position:absolute;top:-9999px;left:-9999px;">
     <h3>Add an image</h3>
     <form action="<?php echo $page->siteRoot;?>a/" method="post" class="post" onsubmit="format.img(this);return false;">
      Enter URI of offsite image:<br />
      <input type="text" name="img_text" value="<?php echo htmlspecialchars($img_text);?>" class="text" /><br />
      <strong>OR</strong> choose an already uploaded image:<br />
      <select name="img_select">
       <option value="---">---</option>
<?php 
$upims=read_dir(ROOT.'images/uploads/','files');
foreach($upims as $upim){
 if(@getimagesize(ROOT.'images/uploads/'.$upim))
 echo "       <option value=\"$upim\"> $upim</option>\n";
}
?>
      </select><br />
      <strong>OR</strong> upload a new image:<br />
      <input type="file" id="img_file" name="img_file" class="file" />
      <p class="center">
       <input type="submit" name="img_add" value="Add" class="button" />
       <input type="button" value="Cancel" class="button" onclick="tb_remove();" />
      </p>
      <h4>Options</h4>
      Alternative text:<br />
      <input type="text" id="img_alttext" name="img_alttext" value="<?php echo htmlspecialchars($img_alttext);?>" class="text" /><br />
      Align:<br />
      <select name="img_float"><option value="---">---</option><option value="left">Left</option><option value="right">Right</option></select>
     </form>
    </div>
<?php 
}
function bbcode($string,$tage=''){
 $string=' '.$string;
 $string=preg_replace("/(<([^>]+)>)/i",'',stripslashes($string));
 $string=preg_replace("/</",'&lt;',$string);
 $strip_nl=array('ol','ul');
 for($s=0;$s<count($strip_nl);$s++){
  while($tmp_str=stristr($string,'['.$strip_nl[$s].']')){
   $tmp_str=substr($tmp_str,0,strpos($tmp_str,'[/'.$strip_nl[$s].']')+6);
   $new_str=preg_replace("/[\n\r]/",'',$tmp_str);
	 $new_str=str_replace('['.$strip_nl[$s].']','['.$strip_nl[$s].'-done]',$new_str);
	 $string=str_replace($tmp_str,$new_str,$string);
  }
  $string=str_replace('['.$strip_nl[$s].'-done]','['.$strip_nl[$s].']',$string);
 }
 $string=preg_replace("/[\n\r]/",'',nl2br($string));
 $string=preg_replace("/<br \/>\s*<br \/>/",'</p><p'.$tage.'>',$string);

 $string=preg_replace("/\[h\](.*?)\[\/h\](<br \/>)?/i",'<h3>$1</h3>',$string);
 $string=preg_replace("/\^(.*?)<br \/>/i",'<h3>$1</h3>',$string);
 $string=preg_replace("/\[b\](.*?)\[\/b\]/i",'<strong>$1</strong>',$string);
 $string=preg_replace("/\[i\](.*?)\[\/i\]/i",'<em>$1</em>',$string);
 $string=preg_replace("/\[u\](.*?)\[\/u\]/i",'<span style="text-decoration:underline;">$1</span>',$string);
 $string=preg_replace("/\[s\](.*?)\[\/s\]/i",'<span style="text-decoration:line-through;">$1</span>',$string);
 $string=preg_replace("/\[sup\](.*?)\[\/sup\]/i",'<sup>$1</sup>',$string);
 $string=preg_replace("/\[sub\](.*?)\[\/sub\]/i",'<sub>$1</sub>',$string);
 $string=preg_replace("/\[align=(.*?)\](.*?)\[\/align\]/i",'<p style="text-align:$1;display:block;">$2</p>',$string);
 $string=preg_replace("/\[color=((#)?[a-zA-Z\d]*)\](.*?)\[\/color\]/i",'<span style="color:$1;background:transparent;">$3</span>',$string);
 $string=preg_replace("/\[bgcolor=((#)?[a-zA-Z\d]*)\](.*?)\[\/bgcolor\]/i",'<span style="background:$1;">$3</span>',$string);
 $string=preg_replace("/\[size=([a-zA-Z\d%]*)\](.*?)\[\/size\]/i",'<span style="font-size:$1;">$2</span>',$string);
 $string=preg_replace("/\[font=([a-zA-Z\-,\' ]*)\](.*?)\[\/font\]/i",'<span style="font-family:$1;">$2</span>',$string);
 $string=imgs($string);
 $string=urls($string);
 $list_a=array("/\[(o|u)l\]/i","/\[\/(o|u)l\]/i",
	             "/\[\*\](.*?)(\[\*\]|%end)/i",
	             "/\[\*\](.*?)(%s-li%|%end)/i",
	 						 "/%start-(.)%/","/%end\-(.)%/",
							 "/%s-li%/","/%e-li%/");
 $list_r=array(' %start-$1% ',' %end-$1% ',
	             ' %s-li%$1%e-li% $2',' %s-li%$1%e-li% $2',
							 '<$1l>','</$1l>',
	 						 '<li>','</li>');
 $string=preg_replace($list_a,$list_r,$string);
 $string=preg_replace("/\[([^\]]+)\]/i",'',$string);
 while(strpos($string,'javascript')!==false)
  $string=str_replace('javascript','',$string);
 return '<p'.$tage.'>'.stripslashes(substr($string,1)).'<br style="clear:both;" /></p>';
}
function imgs($string){
 global $page;
 $string=preg_replace("/\[img(?:=([^\]\|]+))?(?:\|align=([^\]]+))?]((?:(?:http|ftp)(?:s)?\:\/\/)(?:[^\[]+))\[\/img\]/i",'<a href="$3" class="thickbox"><img src="$3" alt="$1" title="$1" class="$2" style="width:250px;" /></a>',$string);
 $string=preg_replace("/\[img(?:=([^\]\|]+))?(?:\|align=([^\]]+))?]www\.([^\[]+)\[\/img\]/i",'<a href="http://www.$3" class="thickbox"><img src="http://www.$3" alt="$1" title="$1" class="$2" style="width:250px;" /></a>',$string);
 $string=preg_replace("/\[img(?:=([^\]\|]+))?(?:\|align=([^\]]+))?]([^\[]+)\[\/img\]/i",'<a href="'.$page->siteRoot.'image?size=m500&amp;file=uploads/$3" class="thickbox"><img src="'.$page->siteRoot.'image?size=m250&amp;file=uploads/$3" alt="$1" title="$1" class="$2" /></a>',$string);
 return $string;
}
function urls($string){
 if(substr($string,0,1)!=' ') $string=' '.$string;
 $string=preg_replace("/\[url=((http|ftp)(s)?\:\/\/)([^\]]+)\](.*?)\[\/url\]/i",'<a href="$1$4" class="link">$5</a>',$string);
 $string=preg_replace("/\[url=www\.([^\]]+)\](.*?)\[\/url\]/i",'<a href="http://www.$1" class="link">$2</a>',$string);
 $string=preg_replace("/\[url=([^\]]+)\](.*?)\[\/url\]/i",'<a href="$1" class="link">$2</a>',$string);
 $string=preg_replace("/([^\"])((http|ftp)(s)?\:\/\/)([-_0-9a-zA-Z\/#]+(\.[-_0-9a-zA-Z\/#\?=&]{1,}){0,})/i",'$1<a href="$2$5" class="link">$2$5</a>',$string);
 $string=preg_replace("/([^\/\"])www\.([-_0-9a-zA-Z\/#]+(\.[-_0-9a-zA-Z\/#\?=&]{1,}){0,})/i",'$1<a href="http://www.$2" class="link">www.$2</a>',$string);
 return $string; 
}
function ago($timestamp,$sub=1){
 $current_time=time();
 $difference=$current_time-$timestamp;
 $periods=array('second','minute','hour','day','week','month','year','decade');
 $lengths=array(1,60,3600,86400,604800,2630880,31570560,315705600);
 for($val=sizeof($lengths)-1;($val>=0) && (($number=$difference/$lengths[$val])<=1);$val--);
 if($val<0) $val=0;
 $new_time=$current_time-($difference%$lengths[$val]);
 $number=floor($number);
 if($number!=1) $periods[$val].="s";
 $text=sprintf("%d %s",$number,$periods[$val]);   
 if($val>=1 && ($current_time-$new_time)>0 && $sub>0){
  $text.=', '.ago($new_time,$sub-1);
 }
 return $text;
}
function clicks($a,$b){
 if($a[0]==$b[0]) return 0;
 return ($a[0]<$b[0])?1:-1;
}
function addslashes_array($a){
 $a_k=array_keys($a);
 $n_a=array();
 foreach($a_k as $a_s){
  if(is_array($a[$a_s])){
	 $n_a[$a_s]=addslashes_array($a[$a_s]);
	}else{
	 $n_a[$a_s]=addslashes(trim($a[$a_s]));
	}
 }
 return $n_a;
}
function strtolower_array($a){
 $a_k=array_keys($a);
 $n_a=array();
 foreach($a_k as $a_s){
  if(is_array($a[$a_s])){
	 $n_a[$a_s]=strtolower_array($a[$a_s]);
	}else{
	 $n_a[$a_s]=strtolower($a[$a_s]);
	}
 }
 return $n_a;
}
function read_dir($start_dir,$type='all'){
 $tree=array();
 if($dir=@opendir($start_dir)){
  while(false!==($file=@readdir($dir))){
   if($file!="." && $file!=".."){
    $abs_file=$start_dir.$file;
    if(is_file($abs_file) && $type!='folders'){
		 if($type=='images'){
		  if(@getimagesize($abs_file)) $tree[]=$file;
     }else
		  $tree[]=$file;
		}elseif(is_dir($abs_file)){
		 if($type!='files' && $type!='images') 
		  $tree[$file]=read_dir($abs_file.'/',$type);
		}
   }
  }
 }
 return $tree;
}

function format_filesize($size){
 $suffix=array('bytes','KB','MB','GB','TB','PB','EB','ZB','YB');
 $factor=1024;
 while($size>$factor){
  $size/=$factor;
	next($suffix);
 }
 return number_format($size,($dp=3-strlen(round($size)))>0?$dp:0).' '.current($suffix);
}

$site_links=array('---'=>'---',''=>'Homepage','about'=>'About','archives'=>'Archives','events'=>'Events &#187;');
foreach($data_events as $event){
 if(is_array($event))
  $site_links['events/'.url_maker($event['title'],$event['date'])]='&nbsp;&nbsp;&nbsp;-&nbsp;'.$event['title'].' ('.$event['date'].')';
}
$site_links=array_merge($site_links,array('events/photos'=>'Event Photos &#187;'));
foreach($data_events as $event){
 if(is_array($event))
  $site_links['events/'.url_maker($event['title'],$event['date']).'#photos']='&nbsp;&nbsp;&nbsp;-&nbsp;'.$event['title'].' photos ('.$event['date'].')';
}
$site_links=array_merge($site_links,array('clubs-and-societies'=>'Clubs','contact'=>'Contact'));
?>