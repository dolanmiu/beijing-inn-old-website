<?php 
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/
if(!defined('BEGIN')){
 die('Die: Hacking attempt');
}

$global=array(
 'maintenance'=>0,
 'cookie_expire'=>60*60*24*100,
 'cookie_path'=>'/'
);

?>