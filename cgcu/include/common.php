<?php 
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/
if(!defined('BEGIN')){
 die('Die: Hacking attempt');
}

putenv("TZ=Europe/London");

function mtime(){
 list($usec,$sec)=explode(" ",microtime());
 return ((float)$usec+(float)$sec);
}
$time_start=mtime();

set_magic_quotes_runtime(0); 

//session_set_cookie_params(0,'/','.cgcu.net'); # <-- Tres importante!!!
session_start();

include(ROOT.'data/pages.php');
include(ROOT.'data/clubs.php');
include(ROOT.'data/events.php');
include(ROOT.'data/links.php');
include(ROOT.'data/emails.php');
include('constants.php');
include('functions.php');
include('session.php');
include('page.php');
?>