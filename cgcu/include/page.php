<?php 
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
if(!defined('BEGIN')){
 die('Die: Hacking attempt');
}

$handle=opendir(ROOT.'data/news/');
while(false!==($file=readdir($handle)))
 if($file!="." && $file!=".."){if(preg_match("/^\d{5}\.php$/",$file)) $newsfiles[]=$file;}
closedir($handle);
if(count($newsfiles)>0)
 rsort($newsfiles);

class Page{
 var $website='www.cgcu.net/'; # The uri of the website
 var $tagline='City &amp; Guilds College Union'; # The page tagline
 var $title='';         # The default page title
 var $pagetitle='Welcome to the CGCU Website';
 var $keywords='';      # Key words for the page
 var $description='';   # Description of the page
 var $styles='';        # Any page only styles
 var $scripts='';       # Any page only scripts
 var $hasbottom=false;  # Is bottom being shown
 
 function Page(){
  $pathinfo=pathinfo($_SERVER['PHP_SELF']);
	$dir=explode('/',$pathinfo['dirname']);
  $this->siteRoot=($_SERVER['DOCUMENT_ROOT']!='/www/htdocs/guilds/'?'/':'/');
  $this->pageDir=str_replace('#'.$this->siteRoot,'','#'.$pathinfo['dirname'].'/');
  $this->pageFile=$pathinfo['basename'];
  $this->pageExt=$pathinfo['extension'];
	$this->url=str_replace(array('#'.$this->siteRoot,strstr($_SERVER['REQUEST_URI'],'?')),'','#'.$_SERVER['REQUEST_URI']);
	$this->isIE=(eregi("MSIE",getenv("HTTP_USER_AGENT"))||eregi("Internet Explorer",getenv("HTTP_USER_AGENT")));
 }
 
 function navon($url){
  $url=strtolower($url);
  return (($url==$this->url||$url.'/'==$this->url)||
	 (preg_match("@$url/(.+)$@",$this->url)&&$url!=''));
 }
 function head($contact=false){
  global $session,$data_clubs,$data_pages;
  ob_start('parse_html');
  $this->title=$this->tagline.($this->title!=''?' / '.$this->title:'');
  $this->styles=is_array($this->styles)?$this->styles:array($this->styles);
  $this->scripts=is_array($this->scripts)?$this->scripts:array($this->scripts);
?>
<!-- Website Design by Michael Haynes [mjh105@imperial.ac.uk] -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
 <head>
  <title><?php echo $this->title;?></title>
  <meta name="title" content="<?php echo $this->title;?>" />
  <meta name="description" content="<?php echo $this->title;?>" />
  <meta name="keywords" content="city, guilds, college, union, imperial, london" />
  <meta name="classification" content="" />
  <meta http-equiv="Content-Language" content="English" />
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <meta http-equiv="content-style-type" content="text/css" />
  <link rel="shortcut icon" href="<?php echo $this->siteRoot;?>favicon.ico" type="image/x-icon" />
  <style type="text/css" media="all">
   <!--
    @import "<?php echo $this->siteRoot;?>css/styles";
    /* Page specific styling */
    <?php echo implode("\n    ",$this->styles)."\n"; ?>
   -->
  </style>
  <!--[if IE]><link rel="stylesheet" href="<?php echo $this->siteRoot;?>css/ie.css" type="text/css" media="all"><![endif]-->
  <!--[if gt IE 6]><link rel="stylesheet" href="<?php echo $this->siteRoot;?>css/ie7.css" type="text/css" media="all"><![endif]-->
  <link rel="stylesheet" href="<?php echo $this->siteRoot;?>css/sIFR-screen.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?php echo $this->siteRoot;?>css/sIFR-print.css" type="text/css" media="print" />
  <script type="text/javascript" src="<?php echo $this->siteRoot;?>js/scripts"></script>
<?php if($session->signed_in){?>
  <script type="text/javascript" src="<?php echo $this->siteRoot;?>js/format.js"></script>
<?php }?>
  <script type="text/javascript">
   <!--
    // Page specific scripts
    <?php echo implode("\n    ",$this->scripts)."\n"; ?>
   -->
  </script>
<?php if($this->siteRoot=='/'){?>
  <script type="text/javascript">
   var gaJsHost=(("https:"==document.location.protocol)?"https://ssl.":"http://www.");
   document.write(unescape("%3Cscript src='"+gaJsHost+"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
  </script>
  <script type="text/javascript">
   var pageTracker=_gat._getTracker("UA-2160432-2");
   pageTracker._initData();
   pageTracker._trackPageview();
  </script>
<?php }?>
 </head>
 <body>
  <div id="header"><div class="wrap">
   <h1><a href="<?php echo $this->siteRoot;?>"><?php echo $this->tagline;?></a></h1>
   <div id="nav">
    <ul>
     <li id="home"><a href="<?php echo $this->siteRoot;?>" class="m<?php echo ($this->navon('')?' on':'');?>"><span>Home</span></a></li>
     <li id="about">
      <a href="<?php echo $this->siteRoot;?>about" class="m<?php echo ($this->navon('about')?' on':'');?>"><span>About</span></a>
<?php if(is_array($data_pages['about'])){?>
      <ul>
<?php  foreach($data_pages['about'] as $ap_url=>$ap_text){?>
       <li><a href="<?php echo $this->siteRoot.'about/'.$ap_url;?>"<?php echo ($this->navon('about/'.$ap_url)?' class="on"':'');?>><span><?php echo $ap_text;?></span></a></li>
<?php  }?>
      </ul>
<?php }?>
     </li>
     <li id="archives"><a href="<?php echo $this->siteRoot;?>archives" class="m<?php echo ($this->navon('archives')?' on':'');?>"><span>Archives</span></a></li>
     <li id="events"><a href="<?php echo $this->siteRoot;?>events" class="m<?php echo ($this->navon('events')?' on':'');?>"><span>Events</span></a></li>
     <li id="clubs">
      <a href="<?php echo $this->siteRoot;?>clubs-and-societies" class="m<?php echo ($this->navon('clubs-and-societies')?' on':'');?>"><span>Clubs &amp; Societies</span></a>
      <ul>
<?php if(is_array($data_clubs['deps'])) foreach($data_clubs['deps'] as $club=>$info){?>
       <li><a href="<?php echo $this->siteRoot.'clubs-and-societies/'.strtolower($club);?>"<?php echo ($this->navon('clubs-and-societies/'.strtolower($club))?' class="on"':'');?>><span><?php echo $club;?></span></a></li>
<?php }?>
       <li><a href="<?php echo $this->siteRoot;?>clubs-and-societies/motor"<?php echo ($this->navon('clubs-and-societies/motor')?' class="on"':'');?>><span>Motor</span></a></li>
       <li><a href="<?php echo $this->siteRoot;?>clubs-and-societies/media"<?php echo ($this->navon('clubs-and-societies/media')?' class="on"':'');?>><span>Media</span></a></li>
      </ul>
     </li>
    </ul>
   </div>
  </div></div>
  <div id="container">
   <div id="content">
    <div id="crufts">
     <ul>
<?php 
  $crufts=strlen($this->url)>0?array_merge(array('home'),explode('/',$this->url)):array('home');
  foreach($crufts as $k=>$cruft){
	 if(empty($cruft) || $crufts[$k-1]=='a' || $crufts[$k-2]=='a')
	  unset($crufts[$k]);
	}
  foreach($crufts as $k=>$cruft){
   if($crufts[$k-1]=='archives' && preg_match("/^\d{5}$/",$cruft))
    $cruft='Post';
   if($k+1<count($crufts)){
?>
      <li id="cruft-<?php echo $cruft;?>"><a href="<?php echo $this->siteRoot.$cruftp.($cruft!='home'?$cruft:'');?>"><?php echo ucwords(str_replace(array('-','soc'),array(' ','Soc'),$cruft=='a'?'admin':$cruft));?></a></li>
<?php
   }else{
?>
      <li id="cruft-<?php echo $cruft;?>"><?php echo ucwords(str_replace(array('-','soc'),array(' ','Soc'),$cruft=='a'?'admin':$cruft));?></li>
<?php
	 } 
   $cruftp.=($cruft!='home'?$cruft.'/':'');
  }
?>
     </ul>
     <h2><?php echo $this->pagetitle;?></h2>
     <br style="clear:both;" />
    </div>
<?php if($contact){?>
    <div id="topcontact">
     <a id="contact<?php echo ($this->navon('contact')?'on':'');?>" href="<?php echo $this->siteRoot;?>contact">Contact</a>
     <br style="clear:both;" />
    </div>
<?php 
  }
 }
 function foot($sidebar=true){
  global $global,$session,$time_start,$newsfiles,$data_links,$data_events;
?>
   </div>
<?php if($sidebar && !(!isset($global['maintenance']) || $global['maintenance'])){?>
   <div id="sidebar">
    <a id="contact<?php echo ($this->navon('contact')?'on':'');?>" href="<?php echo $this->siteRoot;?>contact">Contact</a>
    <br style="clear:both;" />
<?php if($session->signed_in){?>
    <h3>Administration</h3>
    <ul class="red">
     <li><a href="<?php echo $this->siteRoot;?>a/">CGCU Administration</a></li>
     <li><a href="<?php echo $this->siteRoot;?>a/signout">Sign out</a></li>
     <li>Online: <strong><?php echo count($session->stats['online']);?></strong>, Most online: <strong><?php echo $session->stats['most_online'];?></strong></li>
    </ul>
<?php }
$_event=array();
foreach($data_events as $event){
 list($d,$m,$y)=explode('/',$event['date']);
 list($h,$i)=explode(':',$event['time']);
 $etime=mktime($h,$i,0,$m,$d,$y);
 if(!isset($_event)){
  $_event=$event;
	$ctime=$etime;
 }
 if($etime>time()&&($ctime>time()&&$etime<$ctime)||($ctime<time()&&$etime>$ctime)){
  $_event=$event;
	$ctime=$etime;
 }
}
if(count($_event)>0){
?>
    <h3><?php echo (time()>$etime?'Recent':'Upcoming');?> Events</h3>
    <div class="event">
     <a href="<?php echo $this->siteRoot;?>events/<?php echo url_maker($_event['title'],$_event['date']);?>" class="ttl"><?php echo strip_crap($_event['title']);?></a>
     <a href="<?php echo $this->siteRoot;?>events/<?php echo url_maker($_event['title'],$_event['date']);?>" class="img"><img src="<?php echo $this->siteRoot;?>image?size=w80&amp;file=<?php echo ($_event['poster']!=''?'events/'.$_event['poster']:'blank-event.jpg');?>" alt="'<?php echo $_event['title'];?>' poster" /></a>
     <ul>
      <li><strong>When:</strong><br /><?php echo date('jS F, Y',$ctime).' @ '.date('H:ia',$ctime);?></li>
      <li><strong>Where:</strong><br /><?php echo $_event['whereto'];?></li>
      <li><a href="<?php echo $this->siteRoot;?>events/">Find out more &#187;</a></li>
     </ul>
     <?php if($session->signed_in&&0){?> - <a href="<?php echo $this->siteRoot.'a/editevent/'.$_event['id'];?>" class="edit">Edit</a><?php }?>
     <br style="clear:both;" />
    </div>
<?php 
}else{
?>
    <h3>CGCU Events</h3>
    <ul class="error"><li>There are no events!</li></ul>
<?php 
}
?>
    <p class="more"><a href="<?php echo $this->siteRoot;?>events">See all events</a></p>
    <h3>Recent Posts</h3>
<?php 
  if(isset($newsfiles)){
	 $files=array_slice($newsfiles,0,4);
?>
    <ul class="news">
<?php 
	 foreach($files as $file){
    if($contents=@file_get_contents(ROOT.'data/news/'.$file)){
     preg_match('/<!--title\[(.*?)\]-->/',$contents,$title);
     preg_match('/<!--filenum\[(.*?)\]-->/',$contents,$filenum);
     ?>
     <li><a href="<?php echo $this->siteRoot;?>archives/<?php echo $filenum[1];?>" title="<?php echo $title[1];?>"><?php echo char_limit($title[1],40);?></a></li>
<?php
		} 
	 }
   ?>
    </ul>
    <p class="more"><a href="<?php echo $this->siteRoot;?>archives">See more posts</a></p>
<?php 
  }else{
?>
     <ul class="error"><li>Sorry but it appears that there are no society news entries, oh well!</li></ul>
<?php 
  }
  if(count($data_links)>0){
?>
    <h3>Links</h3>
    <ul class="links"><?php 
   foreach($data_links as $link)
    $_li[]=array((int)$link['clicks'],'<li><a href="'.$this->siteRoot.'r/'.$link['id'].'" title="'.$link['url'].' &rarr; '.$link['clicks'].' click'.($link['clicks']==1?'':'s').'">'.$link['text'].'</a>'.$link['description'].'</li>');
   usort($_li,'clicks');
   foreach($_li as $li) echo $li[1];
?></ul>
<?php 
  }
?>
   </div>
<?php }?>
   <br style="clear:both;" />
  </div>
  <div id="footer"><div class="wrap">
   <div id="fphotos">
    <a href="http://www.flickr.com/photos/26516681@N02/" id="flickr"><span>CGCU Photostream</span></a>
<?php 
  if($fdata=@read_url('http://api.flickr.com/services/feeds/photos_public.gne?id=26516681@N02&lang=en-us&format=rss_200')){
   preg_match_all("#<item>(.*?)</item>#is",$fdata,$fdata);
   foreach(array_slice($fdata[1],0,8) as $fph){
    preg_match("#<title>(.*?)</title>#",$fph,$title);$title=$title[1];
    preg_match("#<link>(.*?)</link>#",$fph,$link);$link=$link[1];
    preg_match("#<media:thumbnail url=\"(.*?)\" #",$fph,$thumb);$thumb=$thumb[1];
 ?>
    <a href="<?php echo $link;?>"><img src="<?php echo $thumb;?>" alt="<?php echo $title;?>" title="<?php echo $title;?>" /></a>
<?php 
   }
	}else{
?>
    <p style="text-align:center;line-height:80px;font-size:50px;color:#666;">Feed is<br />broken! =(</p>
<?php 
	}
?>
   </div>
   <div id="flive">
<?php 
  if($livedata=@read_url('http://live.cgcu.net/feeds/rss.php?types')){
?>
    <p><a href="http://live.cgcu.net/" title="Go to the Live! site">Go to the Live! site</a></p>
    <ul>
<?php 
   preg_match_all("#<item>(.*?)</item>#is",$livedata,$livedata);
	 $livedata[1]=array_slice($livedata[1],0,3);
   foreach($livedata[1] as $liveentry){
    preg_match("#<title>(.*?)</title>(?:.*?)<link><!\[CDATA\[(.*?)\]\]></link>(?:.*?)<guid><!\[CDATA\[(.*?)\]\]></guid>(?:.*?)<comments><!\[CDATA\[(.*?)\]\]></comments>(?:.*?)<description>(.*?)</description>(?:.*?)<pubDate>(.*?)</pubDate>(?:.*?) <category>(.*?)</category>(?:.*?)<author>(.*?)</author>(?:.*?)<media:thumbnail url=\"(.*?)\" ></media:thumbnail>#is",$liveentry,$ledata);
    list(,$l_title,$l_link,$l_guid,$l_comments,$l_description,$l_pubDate,$l_category,$l_author,$l_thumbnail)=$ledata;
		list($l_link)=explode('?',$l_link);
 ?>
     <li><span><a href="<?php echo $l_link;?>"><?php echo $l_title;?> </a></span><br /><?php echo $l_description;?></li>
<?php 
   }
?>
    </ul>
<?php 
  }
?>
   </div>
   <div id="fcgcu">
    &copy; <?php echo date('Y');?> City &amp; Guilds College Union - <?php if($session->signed_in){?><a href="/a/">Administration</a> | <a href="/a/signout">Sign out</a><?php }else{?><a href="/a/signin">Sign in</a><?php }?>
   </div>
  </div></div>
 </body>
</html>
<?php 
  ob_end_flush();
 }
 function mail_link($email){
  return '<a href="'.obfuscate('mailto:'.$email).'" class="mail">'.obfuscate($email).'</a>';
 }
}
$page=new Page;
?>
