<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','./');
include(ROOT.'include/common.php');

$page->title='Events';
$page->pagetitle='CGCU Events of Past and Present';

$page->head();

if(isset($_GET['e']) && $_GET['e']=='photos'){
 $folders=read_dir(ROOT.'images/events/','folders');
 $eventphotos=array();
 if(count($folders)>0){
  $folders=array_keys($folders);
  rsort($folders);
  foreach($folders as $folder){
   $index=find_id($data_events,$folder);
   if($index>-1){
    list($d,$m,$y)=explode('/',$data_events[$index]['date']);
    list($h,$i)=explode(':',$data_events[$index]['time']);
    $etime=mktime($h,$i,0,$m,$d,$y);
    if($etime<time()){
 	   $eventphotos[$etime]=array($index,read_dir(ROOT.'images/events/'.$folder.'/','images'));
    }
   }
  }
	if(count($eventphotos)>0){
	 krsort($eventphotos);
	 foreach($eventphotos as $ephotos){
	  list($index,$photos)=$ephotos;
	  $pc=$p=0;
?>
    <h3 style="clear:both;">Photos of '<?php echo $data_events[$index]['title'];?>'</h3>
<?php 
    foreach($photos as $photo){
		 $p++;
		 if(($p>4 && count($photos)<8) || ($p>8)) break;
     if(!@getimagesize(ROOT.'images/events/'.$data_events[$index]['id'].'/'.$photo)) continue;
     $caption=isset($data_captions[$photo])?$data_captions[$photo]:"A photo from '{$data_events[$index]['title']}'";
?>
    <a href="<?php echo $page->siteRoot;?>image?size=m500&amp;file=events/<?php echo $data_events[$index]['id'].'/'.$photo;?>" class="img_tb thickbox" rel="gallery-<?php echo $index;?>">&nbsp;<img src="<?php echo $page->siteRoot;?>image?size=m100&amp;file=events/<?php echo $data_events[$index]['id'].'/'.$photo;?>" alt="<?php echo $caption;?>" title="<?php echo $caption;?>" />&nbsp;</a>
<?php 
    }
?>
    <br style="clear:both;" />
    <p class="more"><a href="<?php echo $page->siteRoot.'events/'.url_maker($data_events[$index]['title'],$data_events[$index]['date']);?>#photos">See more photos</a></p>
<?php 
	 }
  }else{
?>
    <h3>Event Photos</h3>
    <ul class="error"><li>Sorry, there are no event photos to show.</li></ul>
<?php 
	}
 }else{
?>
    <h3>Event Photos</h3>
    <ul class="error"><li>Sorry, there are no event photos to show.</li></ul>
<?php 
 }
}elseif(isset($_GET['e'])){
 
 foreach($data_events as $event)
	if($_GET['e']==url_maker($event['title'],$event['date']))
	 $_event=$event;

 if(isset($_event)){
  list($d,$m,$y)=explode('/',$_event['date']);
  list($h,$i)=explode(':',$_event['time']);
  $time=mktime($h,$i,0,$m,$d,$y);
?>
    <table class="event" cellspacing="0" cellpadding="0" border="0">
     <tr>
      <td style="width:250px;text-align:left;"><a href="<?php echo $page->siteRoot;?>image?size=m500&amp;file=<?php echo ($_event['poster']!=''?'events/'.$_event['poster']:'blank-event.jpg');?>" class="thickbox"><img src="<?php echo $page->siteRoot;?>image?size=w220&amp;file=<?php echo ($_event['poster']!=''?'events/'.$_event['poster']:'blank-event.jpg');?>" alt="'<?php echo $_event['title'];?>' poster" /></a></td>
      <td>
       <p class="top">On <strong><?php echo date("l dS F, Y",$time);?></strong><br /> the City &amp; Guilds College Union (CGCU) presents the</p>
       <h4><?php echo strip_crap($_event['title']);?></h4>
       <p>We will meet at <strong><?php echo date("H:ia",$time);?></strong> in the <?php if($_event['wherefrom']!=''){?><strong><?php echo $_event['wherefrom'];?></strong><br />and from there will make our way to the<br /><?php }?><strong><?php echo $_event['whereto'];?></strong><?php 
  if($_event['description']!=''){?> 
      <?php echo bbcode($_event['description'],' class="description"');?> 
      <?php 
  }
	if($session->signed_in){?><p style="margin-top:10px;"><a href="<?php echo $page->siteRoot.'a/editevent/'.$_event['id'];?>" class="edit">Edit event</a></p><?php }?><br style="clear:both;" />
      </td>
     </tr>
    </table>
    <!--div class="event">
     <img src="<?php echo $page->siteRoot;?>images/image.php?size=w220&amp;file=<?php echo ($_event['poster']!=''?'events/'.$_event['poster']:'blank-event.jpg');?>" alt="'<?php echo $_event['title'];?>' poster" />
     <p class="top">On <strong><?php echo date("l dS F, Y",$time);?></strong><br /> the Imperial Aeronautical Society (IAS) presents the</p>
     <h4><?php echo strip_crap($_event['title']);?></h4>
     <p>We will meet at <strong><?php echo date("H:ia",$time);?></strong> in the <?php if($_event['wherefrom']!=''){?><strong><?php echo $_event['wherefrom'];?></strong><br />and from there will make our way to the<br /><?php }?><strong><?php echo $_event['whereto'];?></strong><?php 
  if($_event['description']!=''){?> 
     <?php echo bbcode($_event['description'],' class="description"');?> 
     <?php 
  }
	if($session->signed_in){?><p style="margin-top:10px;"><a href="<?php echo $page->siteRoot.'a/editevent/'.$_event['id'];?>" class="edit">Edit event</a></p><?php }?><br style="clear:both;" />
    </div-->
    <p class="more"><a href="<?php echo $page->siteRoot;?>events">View all the events</a></p>
    <h4 id="photos">Photos of the Event</h4>
<?php  
  if($time<time()){
	 $photos=read_dir(ROOT.'images/events/'.$_event['id'].'/','images');
	 if(count($photos)>0){
    include(ROOT.'images/events/'.$_event['id'].'/captions.php');
	  foreach($photos as $photo){
     if(!@getimagesize(ROOT.'images/events/'.$_event['id'].'/'.$photo)) continue;
		 $caption=isset($data_captions[$photo])&&!empty($data_captions[$photo])?$data_captions[$photo]:"A photo from '{$event['title']}'";
?>
    <a href="<?php echo $page->siteRoot;?>image?size=m500&amp;file=events/<?php echo $_event['id'].'/'.$photo;?>" class="img_tb thickbox" rel="gallery"><img src="<?php echo $page->siteRoot;?>image?size=m95&amp;file=events/<?php echo $_event['id'].'/'.$photo;?>" alt="<?php echo $caption;?>" title="<?php echo $caption;?>" /></a>
<?php 
    }
?>
    <br style="clear:both;" />
<?php 
   }else{
?>
    <ul class="error"><li>Nobody has uploaded any photos yet! Surely someone remembered a camera...<br />If you have any photo's then contact one of the IAS committee members so they may be uploaded!</li></ul>
<?php 
	 }
  }else{
?>
    <ul class="error"><li>As the event hasn't happened yet there aren't any photos to show!</li></ul>
<?php 
  } 
    if($session->signed_in){
?>
    <p style="text-align:center;margin:5px 0 0 0;"><a href="<?php echo $page->siteRoot;?>a/eventphotos/<?php echo $_event['id'];?>">Upload/Edit Photos</a></p>
<?php 
		}
 }else{
?>
    <h3>Oh Dear Lord</h3>
    <ul class="error"><li>Hmm, the event you're looking for doesn't appear to exist, sorry!</li></ul>
    <p class="more"><a href="<?php echo $page->siteRoot;?>events">View the events that do exist!</a></p>
<?php 
 }
}else{
 foreach($data_events as $event){
	list($d,$m,$y)=explode('/',$event['date']);
	list($h,$i)=explode(':',$event['time']);
	$etime=mktime($h,$i,0,$m,$d,$y);
	if($etime>time()) $_nevents[$etime]=$event;
	else $_oevents[$etime]=$event;
 }
 isset($_nevents)?ksort($_nevents):null;
 isset($_oevents)?krsort($_oevents):null;
 
 if($pun_user['g_id']==PUN_ADMIN){
?>
    <p style="margin:0 0 10px 0;text-align:center;"><a href="<?php echo $page->siteRoot;?>a/addevent" class="edit">Add an event</a></p>
<?php 
 }
?>
    <h3>Upcoming Events</h3>
<?php 
 if(count($_nevents)>0)
  foreach($_nevents as $n=>$event) show_event($event,$n<count($_nevents)-1);
 else{
?>
    <ul class="error"><li>Unfortunately there are no upcoming events =(<br />If you feel this is wrong and there should be many exciting things planned then feel free to contact <?php echo '<a href="'.$page->siteRoot.'contact">the CGCU</a>';?> to voice your concerns!</li></ul>
<?php 
 }
?>
    <div class="hr"></div>
    <h3>Events that have Happened</h3>
<?php 
 if(count($_oevents)>0)
  foreach($_oevents as $n=>$event) show_event($event,$n<count($_oevents)-1);
 else{
?>
    <ul class="error"><li>No events have happened yet, hopefully this means that there is lots of fun and drinking to look forward to!</li></ul>
<?php 
 }
}
function show_event($event,$border){
 global $page,$pun_user;
	list($d,$m,$y)=explode('/',$event['date']);
	list($h,$i)=explode(':',$event['time']);
	$time=mktime($h,$i,0,$m,$d,$y);
?>
    <div class="event_small<?php echo ($border?' event_small_b':'');?>">
     <a href="<?php echo $page->siteRoot;?>events/<?php echo url_maker($event['title'],$event['date']);?>" class="img"><img src="<?php echo $page->siteRoot;?>image?size=w80&amp;file=<?php echo ($event['poster']!=''?'events/'.$event['poster']:'blank-event.jpg');?>" alt="'<?php echo $event['title'];?>' poster" /></a>
     <ul>
      <li>What:<br /><strong><a href="<?php echo $page->siteRoot;?>events/<?php echo url_maker($event['title'],$event['date']);?>"><?php echo strip_crap($event['title']);?></a></strong><?php if($pun_user['g_id']==PUN_ADMIN){?> - <a href="<?php echo $page->siteRoot.'a/editevent/'.$event['id'];?>" class="edit">Edit</a><?php }?></li>
      <li>When:<br /><strong><?php echo date('jS F, Y',$time).' @ '.date('H:ia',$time);?></strong></li>
      <li>Where:<br /><strong><?php echo $event['whereto'];?></strong></li>
     </ul>
     <br style="clear:both;" />
    </div>
<?php 
}
$page->foot();
?>
