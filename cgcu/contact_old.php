<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','./');
include(ROOT.'include/common.php');
include(ROOT.'include/email.php');

$page->title='Contact';
$page->pagetitle='Contact the CGCU';

$page->head();

if(isset($_POST['submit'])){
 $name=trim(stripslashes(@$_POST['name']));
 $email=trim(stripslashes(@$_POST['email']));
 $subject=trim(stripslashes(@$_POST['subject']));
 $message=trim(stripslashes(@$_POST['message']));
 
 if(empty($email) || empty($subject) || empty($message))
  $err='Please fill in all the required fields';

 if(!isset($err)){
  $body='<html><body><font face="arial" size="2">'.
	      (!empty($name)?'<p><font color="#888888">Name:</font><br />'.$name.'</p>':'').
	      '<p><font color="#888888">Email:</font><br />'.$email.'</p>'.
	      '<p><font color="#888888">Subject:</font><br />'.$subject.'</p>'.
				'<p><font color="#888888">Message:</font><br />'.$message.'</p>'.
				'<p><font color="#888888">(IP: '.$_SERVER['REMOTE_ADDR'].')</font></p>'.
				'</font></body></html>';
  send_mail('foepres@ic.ac.uk','[CGCU-Website] Contact Form Message ('.date('d/m/Y H:i:s').')',$body,(!empty($name)?'"'.$name.'" ':'')."<{$email}>");
  if(send_mail(implode(',',$contact_emails),'[CGCU-Website] Contact Form Message ('.date('d/m/Y H:i:s').')',$body,(!empty($name)?'"'.$name.'" ':'')."<{$email}>")){
?>
    <h3>Contact: Message sent</h3>
    <ul class="success"><li>Your message has been sent!</li></ul>
<?php 
   $page->foot();
   exit;
	}else{
   $err='The message could not be sent, please try again later.';
	}
 }
}elseif(isset($_GET['subject'])){
 $subject=$_GET['subject'];
 if($subject=='Bug Report'){
  $message="URI: {$_SERVER['HTTP_REFERER']}\n---\nBug: ";
 }
}
?>
    <h3>Contact Form &amp; Email</h3>
<?php if($err!=''){?>
    <ul class="error"><li><?php echo $err;?></li></ul>
<?php }?>
    <form action="<?php echo $page->siteRoot;?>contact" method="post" class="post">
     <label for="name">Your name (not required):</label><br />
     <input type="text" id="name" name="name" value="<?php echo htmlspecialchars($name);?>" class="text" /><br />
     <label for="email">Your email:</label><br />
     <input type="text" id="email" name="email" value="<?php echo htmlspecialchars($email);?>" class="text" /><br />
     <label for="subject">Subject:</label><br />
     <input type="text" id="subject" name="subject" value="<?php echo htmlspecialchars($subject);?>" class="text" /><br />
     <label for="message">Message:</label><br />
     <textarea id="message" name="message" rows="5" cols="40" class="text"><?php echo htmlspecialchars($message);?></textarea>
     <p class="center" style="margin-bottom:0;">
      <input type="submit" name="submit" value="Send" class="button" />
     </p>
    </form>
    <p style="margin-top:20px;">The general City & Guilds College Union email address is: <?php echo $page->mail_link('guilds@cgcu.net');?></p>
    <div class="hr"></div>
    <p>If you have a more specific query, you can try contacting one of the Union Officers directly. Also, if you are an Imperial College student of staff member, you can use our Intranet, which has a larger list of contact details.</p>
    <h3>Postal Address</h3>
    <p>Please send all postal correspondence to the City & Guilds College Union to the following address:</p>
    <ul><li>City & Guilds College Union<br />Room 340 Mechanical Engineering<br />Imperial College<br />London<br />SW7 2BX</li></ul>
    <p>If you are an Imperial College student or staff member, you can also send items to us through the internal mail. Simply address it to City &amp; Guilds College Union, Room 340 Mechanical Engineering, and place it in internal post.</p>
    <h3>Phone &amp; Fax</h3>
    <p>You can also phone and fax us using the numbers below. Office hours are normally between 10am and 5pm, Monday to Friday (excluding bank holidays and college closures).
    <ul><li>Phone: 020 759 48073 (Internal ext. 48073)</li><li>Fax: 020 759 48095</li></ul>
<?php 
$page->foot();
?>