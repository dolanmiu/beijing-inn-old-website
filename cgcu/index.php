<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','./');
include(ROOT . 'include/common.php');

$page->scripts=array("stepcarousel.setup({"," galleryid:'iccarousel',"," beltclass:'belt',"," panelclass:'icnews',",
" statusvars:['ca','cb','cc'],"," contenttype:['inline']",
"})","setInterval(function(){"," stepcarousel.stepBy('iccarousel',1);","},5000)");

$page->head();
?>
    The City and Guilds College Union (CGCU) is the student voice of the Faculty of Engineering and the Tanaka Business School at Imperial College in London. We are part of the Imperial College Union, which represents all 12,000 students at Imperial.
    <div class="hr"></div>
<?php 
if(isset($newsfiles)){
 $files=array_slice($newsfiles,0,1);
 foreach($files as $file){
  if($contents=@file_get_contents(ROOT.'data/news/'.$file)){
   preg_match('/<!--title\[(.*?)\]-->/',$contents,$title);
   preg_match('/<!--datetime\[(.*?)\]-->/',$contents,$date);
   preg_match('/<!--author\[(.*?)\]-->/',$contents,$author);
   list($author,$author_id)=explode('|',$author[1]);
   preg_match('/<!--filenum\[(.*?)\]-->/',$contents,$filenum);
   preg_match('/<!--entry_text-->(.*?)<!--end_entry_text-->/s',$contents,$text);
   preg_match('/<!--comments_on\[(.*?)\]-->/',$contents,$comments_on);
   preg_match_all('/<!--comment_start--><!--comment_allowed\[1\]-->(.*?)<!--comment_end-->/s',$contents,$comments);
?>
    <h3 class="news"><a href="<?php echo $page->siteRoot;?>archives/<?php echo str_replace('.php','',$file);?>"><?php echo $title[1];?></a></h3>
    <p class="news">
     <span class="newsdate">Posted <?php echo date('F jS, Y \a\t H:ia',$date[1]);?></span>
     <span class="newscomments"><?php $nc=count($comments[1]);echo ($comments_on[1]=='1'?'<a href="'.$page->siteRoot.'archives/'.$filenum[1].'#comments">'.($nc>0?$nc:'No').' comment'.($nc==1?'':'s').'</a>':'Comments are closed');?> - by <?php echo $author;?></span>
     <br style="clear:both;" />
    </p>
    <?php echo bbcode(word_limit($text[1]),' class="newstext"');?> 
<?php if(str_word_count($text[1])>100){?>
    <p class="continue"><a href="<?php echo $page->siteRoot.'archives/'.$filenum[1];?>">Continue reading</a></p>
<?php }
	    if($session->is_admin()){?>
    <p style="margin-top:10px;"><a href="<?php echo $page->siteRoot.'a/editnews/'.$filenum[1];?>" class="edit">Edit item</a></p>
<?php }?>
<?php #if(!isset($_br)){echo "    <br />\n";}$_br=true;
  }
 }
?>
    <p class="more"><a href="<?php echo $page->siteRoot;?>archives">View the news archive</a></p>
<?php }else{?>
    <h3>Recent News</h3>
    <ul class="error"><li>There is no society news, how boring!</li></ul>
<?php }
$_event=array();
foreach($data_events as $event){
 list($d,$m,$y)=explode('/',$event['date']);
 list($h,$i)=explode(':',$event['time']);
 $etime=mktime($h,$i,0,$m,$d,$y);
 if(!isset($_event)){
  $_event=$event;
	$ctime=$etime;
 }
 if($etime>time()&&($ctime>time()&&$etime<$ctime)||($ctime<time()&&$etime>$ctime)){
  $_event=$event;
	$ctime=$etime;
 }
}
if(count($_event)>0){
?>
    <div class="hr"></div>
    <h3>Upcoming Events</h3>
    <div class="event_small">
     <a href="<?php echo $page->siteRoot;?>events/<?php echo url_maker($_event['title'],$_event['date']);?>" class="img"><img src="<?php echo $page->siteRoot;?>image?size=w120&amp;file=<?php echo ($_event['poster']!=''?'events/'.$_event['poster']:'blank-event.jpg');?>" alt="'<?php echo $_event['title'];?>' poster" /></a>
     <ul style="width:340px;">
      <li>What:<br /><strong><a href="<?php echo $page->siteRoot;?>events/<?php echo url_maker($_event['title'],$_event['date']);?>"><?php echo strip_crap($_event['title']);?></a></strong><?php if($session->signed_in){?> - <a href="<?php echo $page->siteRoot.'a/editevent/'.$_event['id'];?>" class="edit">Edit</a><?php }?></li>
      <li>When:<br /><strong><?php echo date('jS F, Y',$ctime).' @ '.date('H:ia',$ctime);?></strong></li>
      <li>Where:<br /><strong><?php echo $_event['whereto'];?></strong></li>
      <li>What for:<br /><strong><?php echo bbcode($_event['description']);?></strong></li>
     </ul>
     <br style="clear:both;" />
    </div>
    <p class="more"><a href="<?php echo $page->siteRoot;?>events">See more events</a></p>
<?php 
}
?>
    <div class="hr"></div>
    <h3><a href="http://www3.imperial.ac.uk/news">Imperial College News</a></h3>
    <div id="iccarousel" class="stepcarousel">
     <div class="belt">
<?php 
$data=fetch_page('http://www3.imperial.ac.uk/news');
list(,$data)=explode('<link href="/css/college.css" rel="stylesheet" type="text/css" />',$data);
list($data)=explode('<!-- show footer template',$data);
$data=preg_replace(array("/[\t\r\n]/","/\s+/","/\"(\s+)>/"),array('',' ','">'),$data);
preg_match_all("#<td><img width=\"63\" height=\"63\" src=\"(.*?)\"(?:.*?)alt=\"(.*?)\"></td></tr>(?:<tr><td class=\"captiontext\"></td></tr>)?</table></td><td(?:.*?)><!--(?:.*?)--><a href=\"/portal/page\?_pageid=(.*?)\"(?: target=\"_new1\")?>(.*?)</a><br>(?:\s*?)<span class=\"graytext\">(.*?)</span><br>(?:</td></tr><tr><td colspan=2>)?(.*?)</td>#",$data,$matches);

for($m=0;$m<count($matches[1]);$m++){
 $img=str_replace('_small','_medium',"http://www3.imperial.ac.uk{$matches[1][$m]}");
 $alt=$matches[2][$m];
 $url=str_replace('&','&amp;',"http://www3.imperial.ac.uk/portal/page?_pageid={$matches[3][$m]}");
 $ttl=strip_crap($matches[4][$m]);
 $dte=strip_crap($matches[5][$m]);
 $dsc=strip_crap($matches[6][$m]);
?>
      <div class="icnews">
       <h4><a href="<?php echo $url;?>"><?php echo $ttl;?></a></h4>
       <a href="<?php echo $url;?>"><img src="<?php echo $img;?>" alt="<?php echo $alt;?>" title="<?php echo $alt;?>" style="width:100px;height:100px;" /></a>
       <p class="dsc"><?php echo $dsc;?></p>
       <p class="dte"><?php echo $dte;?></p>
       <br style="clear:both;" />
      </div>
<?php 
}
?>
     </div>
    </div>
<?php 
$page->foot();
?>
