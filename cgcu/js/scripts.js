
var scripts={
 start:function(){
  $('a').focus(function(){this.blur();});
	
  if($('nav')) scripts.init_nav();
 },
 init_nav:function(){
  var hov=[];
  $('#nav>ul>li:has(ul)')
   .mouseover(function(){
    hov[this.id]=true;
    if($('ul:hidden',this).length>0 && !$('ul:animated',this).length){
     $('ul',this).show();//.slideDown(250);//
    }
   })
   .mouseout(function(i){
    var obj=this;
    hov[obj.id]=false;
    setTimeout(function(){
     if(!hov[obj.id] && $('ul:visible',obj).length){
      $('ul',obj).hide();//.slideUp(250);//
     }
    },25);
   })
   .find('ul').mouseover(function(){
    hov[$(this).parent().attr('id')]=true;
   });

  imgLoader=new Image();
  imgLoader.src=tb_pathToImage;
 }
};
$(document).ready(scripts.start);


Array.prototype.in_array=function(obj){
 var len=this.length;
 for(var x=0;x<=len;x++){
  if(this[x]==obj) return true;
 }
 return false;
}	