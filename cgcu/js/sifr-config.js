var myriadpro={
 src:'/cgcu/fonts/myriad-pro.swf'
};

sIFR.activate(myriadpro);

sIFR.replace(myriadpro, {
 selector:'#content h3',
 css:[
  '.sIFR-root{color:#810303;background-color:#ffffff}',
  'a{text-decoration:none;}',
  'a:link{color:#810303;}',
  'a:hover{color:#000000;}'
 ],
 wmode:'transparent'
});
sIFR.replace(myriadpro, {
 selector:'#content h4',
 css:[
  '.sIFR-root{color:#000000;background-color:#ffffff}',
  'a{text-decoration:none;}',
  'a:link{color:#000000;}',
  'a:hover{color:#810303;}'
 ],
 wmode:'transparent'
});
sIFR.replace(myriadpro, {
 selector:'#sidebar h3',
 css:[
  '.sIFR-root{font-size:13px;color:#810303;background-color:#ffffff;text-align:center;}'
 ],
 wmode:'transparent',
 offsetTop:1,
 tuneHeight:-4
});
sIFR.replace(myriadpro, {
 selector:'div#footer div#flive ul li span',
 css:[
  '.sIFR-root{font-size:11px;color:#810303;}',
  'a{text-decoration:none;}',
  'a:link{color:#810303;}',
  'a:hover{color:#000000;}'
 ],
 wmode:'transparent',
 tuneWidth:10,
 tuneHeight:-6
});


