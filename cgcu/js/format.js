var format={
 ipath:'',
 toolbar_id:'',
 textarea_id:'',
 tools:[],
 start:'',
 middle:'',
 end:'',
 load:function(ipath,toolbar_id,textarea_id){
  if(format.tools.length<=0||ipath==''||toolbar_id==''||textarea_id=='') return false;
	else{
	 format.ipath=ipath;
	 format.toolbar_id=toolbar_id;
	 format.textarea_id=textarea_id;
	}
  $('#'+format.toolbar_id).html(format.toolbar());
 },
 toolbar:function(){
  var html='',i;
  for(i=0;i<format.tools.length;i++){
	 option=format.tools[i]['option']?format.tools[i]['option']:'';
	 html+='<a href="#" onclick="format.tool(\''+format.tools[i]['fn']+'\',\''+option+'\');return false;" '+
	       'title="'+format.tools[i]['text']+'"><img src="'+format.ipath+format.tools[i]['image']+'" style="margin:0;" /></a>';
	}
	return html+'&nbsp;';
 },
 tool:function(f,option){
  var text='',tag='',option=(option&&option!=''?'='+option:''),filler='';
  switch(f){
   case 'bold':tag='b';break;
	 case 'italic':tag='i';break;
	 case 'underline':tag='u';break;
	 case 'strike':tag='s';break;
	 case 'align':tag='align';break;
   case 'ol':tag='ol';option="]\n [*";filler=" \n";break;
	 case 'ul':tag='ul';option="]\n [*";filler=" \n";break;
   case 'url':tag='url';filler='text here';break;
   case 'color':tag='colour';break;
	 case 'bgcolor':tag='hilite';break;
   default:tag=f;break;
  }
  if(tag==''){
	 setTimeout('format.focus();',10);
	 return;
	}
	format.start=format.middle=format.end='';
	if(tag=='url' && !document.all && $('form','#uridiv').length){
   $('form','#uridiv')[0].reset();
	 if(($('#'+format.textarea_id)[0].selectionStart || $('#'+format.textarea_id)[0].selectionStart=='0')){
	  format.start_selection=$('#'+format.textarea_id)[0].selectionStart;
		format.end_selection=$('#'+format.textarea_id)[0].selectionEnd;
	  format.start=($('#'+format.textarea_id).val()).substring(0,format.start_selection);
	  format.middle=($('#'+format.textarea_id).val()).substring(format.start_selection,format.end_selection);
	  format.end=($('#'+format.textarea_id).val()).substring(format.end_selection,$('#'+format.textarea_id)[0].textLength);
	 }
	 if(format.middle!='')
	  $('#uri_text','#uridiv>form').attr('value',format.middle);
	 tb_show('','#TB_inline?height=240&width=500&inlineId=uridiv','');
	 $('form input','#TB_window')[0].focus();
	}else if(tag=='img' && $('form','#imgdiv').length){
   $('form','#imgdiv')[0].reset();
	 if(($('#'+format.textarea_id)[0].selectionStart || $('#'+format.textarea_id)[0].selectionStart=='0')){
	  format.start_selection=$('#'+format.textarea_id)[0].selectionStart;
		format.end_selection=$('#'+format.textarea_id)[0].selectionEnd;
	  format.start=($('#'+format.textarea_id).val()).substring(0,format.start_selection);
	  format.middle=($('#'+format.textarea_id).val()).substring(format.start_selection,format.end_selection);
	  format.end=($('#'+format.textarea_id).val()).substring(format.end_selection,$('#'+format.textarea_id)[0].textLength);
	 }
	 if(format.middle!='')
	  $('#img_alttext','#imgdiv>form').attr('value',format.middle);
	 tb_show('','#TB_inline?height=300&width=500&inlineId=imgdiv','');
   $('form input','#TB_window')[0].focus();
	}else if(document.selection && document.selection.createRange){
	 str=document.selection.createRange().text;
	 format.focus();
	 sel=document.selection.createRange();
	 sel.text='['+tag+option+']'+str+filler+'[/'+tag+']';
	 format.focus();
  }else if(($('#'+format.textarea_id)[0].selectionStart || $('#'+format.textarea_id)[0].selectionStart=='0')){
	 var start_selection=$('#'+format.textarea_id)[0].selectionStart,end_selection=$('#'+format.textarea_id)[0].selectionEnd;
	 var start=($('#'+format.textarea_id).val()).substring(0,start_selection);
	 var middle=($('#'+format.textarea_id).val()).substring(start_selection,end_selection);
	 var end=($('#'+format.textarea_id).val()).substring(end_selection,$('#'+format.textarea_id)[0].textLength);
	 if(middle.length>0)
	  filler='';
	 if(middle.substring(0,tag.length+2)=='['+tag+']'&&middle.substring(middle.length-tag.length-3,middle.length)=='[/'+tag+']'){
	  middle=middle.substring(tag.length+2,middle.length-tag.length-3);
	 }else{
	  middle='['+tag+option+']'+middle+filler+'[/'+tag+']';
	 }
	 $('#'+format.textarea_id).val(start+middle+end);
	 setTimeout("format.focus();$('#"+format.textarea_id+"')[0].selectionStart="+(start_selection+tag.length+option.length+2)+";"+
	 "$('#"+format.textarea_id+"')[0].selectionEnd="+(start_selection+middle.length-tag.length-3-(filler.substring(filler.length-1,filler.length)=="\n"?1:0))+";",10);
  }else{
	 $('#'+format.textarea_id).val($('#'+format.textarea_id).val()+'['+tag+option+']'+filler+'[/'+tag+']');
	 setTimeout("format.focus();$('#"+format.textarea_id+"')[0].selectionStart="+($('#'+format.textarea_id).val().length-tag.length-3)+";$('#"+format.textarea_id+"')[0].selectionEnd="+($('#'+format.textarea_id).val().length-tag.length-3),10);
  }
 },
 focus:function(){
  $('#'+format.textarea_id)[0].focus();
 },
 url:function(form){
  var err='';
  if(form.uri_text.value=='')
	 err='You need to fill in the text field!';
	
	if(form.uri_uri.value=='' && form.uri_select[form.uri_select.selectedIndex].value=='/cgcu/---' && err=='')
	 err='You need to fill in or choose a link!';
	
	if(err!=''){
	 alert(err);
	 form.elements[0].focus();
	}else{
	 if(form.uri_uri.value!='')
	  link_uri=form.uri_uri.value;
	 else
	  link_uri=form.uri_select[form.uri_select.selectedIndex].value;
	
	 link_text=form.uri_text.value;
	 
	 if(format.start!='' || format.middle!='' || format.end!=''){
    if(format.middle.substring(0,5)=='[url]' && format.middle.substring(format.middle.length-6,format.middle.length)=='[/url]')
     format.middle=format.middle.substring(5,format.middle.length-6);
    else
     format.middle='[url='+link_uri+']'+link_text+'[/url]';
    $('#'+format.textarea_id).val(format.start+format.middle+format.end);
    setTimeout("format.focus();$('#"+format.textarea_id+"')[0].selectionStart="+(format.start_selection+link_uri.length+6)+";"+
    "$('#"+format.textarea_id+"')[0].selectionEnd="+(format.start_selection+format.middle.length-6)+";",10);
   }else{
    $('#'+format.textarea_id).val($('#'+format.textarea_id).val()+'[url='+link_uri+']'+link_text+'[/url]');
    setTimeout("format.focus();$('#"+format.textarea_id+"')[0].selectionStart="+($('#'+format.textarea_id).val().length-6)+";$('#"+format.textarea_id+"')[0].selectionEnd="+($('#'+format.textarea_id).val().length-6),10);
   }
	 tb_remove();
	}
 },
 img:function(form){
	if(form.img_text.value=='' && form.img_select[form.img_select.selectedIndex].value=='---' && form.img_file.value=='')
	 alert('You need to specify some form of picture!');
	else{
	 var is_img=new RegExp('^.+\.(jpe?g|gif|png)$','i');
	 if(form.img_text.value!='' || form.img_select[form.img_select.selectedIndex].value!='---'){
	  img_file=form.img_text.value!=''?form.img_text.value:form.img_select[form.img_select.selectedIndex].value;
	  if(!is_img.test(img_file)){
  	 alert('You need to specify a valid image!');
		 return false;
	  }
	 }else{
    $("#loading").ajaxStart(function(){$(this).show();}).ajaxComplete(function(){$(this).hide();});
    $.ajaxFileUpload({
     url:'/cgcu/a/ajaxupload.php', 
     secureuri:false,
     fileElementId:'img_file',
     dataType:'json',
     success:function(data,status){
      if(typeof(data.error)!='undefined'){
       if(data.error!='') alert(data.error);
       else{
			  form.img_text.value=data.msg;
				format.img(form);
			 }
      }
     },
     error:function(data,status,e){alert(e);}
    });
		return false;
   }  
	 var alt_text=form.img_alttext.value!=''?'='+form.img_alttext.value:'';
	 var align=form.img_float[form.img_float.selectedIndex].value;
	 alt_text+=(align!='---'?'|align='+align:'');
	 if(format.start!='' || format.middle!='' || format.end!=''){
    if(format.middle.substring(0,5)=='[img]' && format.middle.substring(format.middle.length-6,format.middle.length)=='[/img]')
     format.middle=format.middle.substring(5,format.middle.length-6);
    else
     format.middle='[img'+alt_text+']'+img_file+'[/img]';
    $('#'+format.textarea_id).val(format.start+format.middle+format.end);
    setTimeout("format.focus();$('#"+format.textarea_id+"')[0].selectionStart="+(format.start_selection+alt_text.length+5)+";"+
    "$('#"+format.textarea_id+"')[0].selectionEnd="+(format.start_selection+format.middle.length-6)+";",10);
   }else{
    $('#'+format.textarea_id).val($('#'+format.textarea_id).val()+'[img'+alt_text+']'+img_file+'[/img]');
    setTimeout("format.focus();$('#"+format.textarea_id+"')[0].selectionStart="+($('#'+format.textarea_id).val().length-img_file.length-6)+";$('#"+format.textarea_id+"')[0].selectionEnd="+($('#'+format.textarea_id).val().length-6),10);
   }
	 tb_remove();
	}
 }
}

