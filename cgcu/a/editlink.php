<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','../');
include(ROOT.'include/common.php');

if(!$session->signed_in){
 header('Location: '.$page->siteRoot.'a/signin');
 exit;
}

if(isset($_POST['submit'])){
 $index=find_id($data_links,(int)$_GET['p1']);
 if($index>-1){
  $url=trim(stripslashes(@$_POST['url']));
  $text=trim(stripslashes(@$_POST['text']));
  $description=trim(stripslashes(@$_POST['description']));
 
  if(empty($url) || empty($text) || empty($description))
   $err='empty';

  if(!isset($err)){
	 $data_links[$index]['url']=$url;
	 $data_links[$index]['text']=$text;
	 $data_links[$index]['description']=$description;
	 recache_arr('$data_links',$data_links,ROOT.'data/links.php');
   header('Location: '.$page->siteRoot.'a/editlink');
   exit;
  }
 }else{
  $err='noexist';
 }
}else{
 $url='http://';
}
if(isset($_POST['delete'])){
 $index=find_id($data_links,(int)$_GET['p1']);
 if($index>-1){
  unset($data_links[$index]);
  recache_arr('$data_links',$data_links,ROOT.'data/links.php');
 
  header('Location: '.$page->siteRoot.'a/editlink');
  exit;
 }else{
  $err='noexist';
 }
}

$page->title='Administration &#187; Edit a Link';
$page->pagetitle='Edit a Link Resource';
$page->head();

if(isset($_GET['p1'])){
 $index=find_id($data_links,(int)$_GET['p1']);
 if($index>-1){
  if(!isset($_POST['submit'])){
	 $url=$data_links[$index]['url'];
	 $text=$data_links[$index]['text'];
	 $description=$data_links[$index]['description'];
  }
?>
    <h3>Edit a Link Resource</h3>
<?php if($err=='empty'){?>
    <ul class="error"><li>You need to fill in all of the fields!</li></ul>
<?php }elseif($err=='url'){?>
    <ul class="error"><li>You need to give a valid url!</li></ul>
<?php }elseif($err=='noexist'){?>
    <ul class="error"><li>The link you want to edit does not exist!</li></ul>
<?php }?>
    <form action="<?php echo $page->siteRoot.'a/editlink/'.$_GET['p1'];?>" method="post" class="post">
     Link URI:<br />
     <input type="text" name="url" value="<?php echo $url;?>" class="text" /><br />
     Link text:<br />
     <input type="text" name="text" value="<?php echo $text;?>" class="text" /><br />
     Description:<br />
     <textarea name="description" rows="5" cols="40" class="text"><?php echo $description;?></textarea>
     <p class="center">This link was added on <strong><?php echo date('d/m/Y H:i:s',(int)$data_links[$index]['datetime']);?></strong> by <strong><?php echo $data_links[$index]['author'];?></strong> and has had <strong><?php echo $data_links[$index]['clicks'];?> click<?php echo ($data_links[$index]['clicks']==1?'':'s');?></strong>.</p>
     <p class="center">
      <input type="submit" name="delete" value="Delete" class="button" onclick="return confirm('Are you sure you want to delete this link?');" />
      <input type="submit" name="submit" value="Edit" class="button" />
      <input type="button" value="Cancel" class="button" onclick="window.location='<?php echo $page->siteRoot;?>a/editlink'" />
     </p>
    </form>
<?php  
 }else{
?>
    <h3>Link cannot be found</h3>
    <ul class="error"><li>Sorry but the link you want to edit just isn't there.</li></ul>
<?php 
 }
}else{
?>
    <h3>Choose a Link to Edit</h3>
<?php  
 if(count($data_links)>0){
?>
    <ul class="links"><?php 
foreach($data_links as $link){
 ?><li><a href="<?php echo $page->siteRoot.'a/editlink/'.$link['id'];?>"><?php echo $link['text'];?> <span>&rarr; <?php echo $link['url'];?> (<?php echo $link['clicks'];?> click<?php echo ($link['clicks']==1?'':'s');?>)</span></a><span class="description"><?php echo $link['description'];?></span></li><?php 
}
?></ul>
<?php  
 }else{?>
    <ul class="error"><li>There are no useful links.</li></ul>
<?php }
}
$page->foot();
?>