<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','../');
include(ROOT.'include/common.php');

if(!$session->signed_in){
 header('Location: '.$page->siteRoot.'a/signin');
 exit;
}

if(isset($_POST['submit'])){
 $url=trim(stripslashes(@$_POST['url']));
 $text=trim(stripslashes(@$_POST['text']));
 $description=trim(stripslashes(@$_POST['description']));

 if(empty($url) || empty($text) || empty($description))
  $err='empty';

 if(!isset($err)){
	$id=$data_links[count($data_links)-1]['id']+1;
	$data_links[]=array(
   'id'=>$id,
   'datetime'=>time(),
   'author'=>$session->username,
   'url'=>$url,
   'text'=>$text,
   'description'=>$description,
   'clicks'=>'0'
  );
  recache_arr('$data_links',$data_links,ROOT.'data/links.php');
  header('Location: '.$page->siteRoot.'a/editlink');
  exit;
 }
}else{
 $url='http://';
}

$page->title='Administration &#187; Add a Link';
$page->pagetitle='Add a Link Resource';
$page->head();
?>
    <h3>Add a Link Resource</h3>
<?php if($err=='empty'){?>
    <ul class="error"><li>You need to fill in all of the fields!</li></ul>
<?php }elseif($err=='url'){?>
    <ul class="error"><li>You need to give a valid url!</li></ul>
<?php }?>
    <form action="<?php echo $page->siteRoot.'a/addlink';?>" method="post" class="post">
     Link URI:<br />
     <input type="text" name="url" value="<?php echo htmlspecialchars($url);?>" class="text" /><br />
     Link text:<br />
     <input type="text" name="text" value="<?php echo htmlspecialchars($text);?>" class="text" /><br />
     Description:<br />
     <textarea name="description" rows="5" cols="40" class="text"><?php echo htmlspecialchars($description);?></textarea>
     <p class="center">
      <input type="submit" name="submit" value="Save" class="button" />
      <input type="button" value="Cancel" class="button" onclick="window.location='<?php echo $page->siteRoot;?>a/'" />
     </p>
    </form>
<?php  
$page->foot();
?>