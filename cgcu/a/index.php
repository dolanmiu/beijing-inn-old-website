<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','../');
include(ROOT.'include/common.php');

$page->title='Administration';
$page->pagetitle='Sign in to the CGCU website';

if($session->signed_in){
 $page->head();
?>
    <h3>Hey <?php echo $session->username;?>, what would you like to do?</h3>
    <ul><?php  
		 ?><li><a href="<?php echo $page->siteRoot;?>a/addnews">Add a News Item</a></li><?php  
		 ?><li><a href="<?php echo $page->siteRoot;?>a/editnews">Edit a News Item</a></li><?php  
		 ?><li><a href="<?php echo $page->siteRoot;?>a/newscomments">Moderate News Items Comments</a></li><?php  
		 ?><li class="space"><a href="<?php echo $page->siteRoot;?>a/addevent">Add an Event</a></li><?php  
		 ?><li><a href="<?php echo $page->siteRoot;?>a/editevent">Edit an Event</a></li><?php  
		 ?><li><a href="<?php echo $page->siteRoot;?>a/eventphotos">Edit Event Photos</a></li><?php  
		 ?><li class="space"><a href="<?php echo $page->siteRoot;?>a/addlink">Add a Link Resource</a></li><?php  
		 ?><li><a href="<?php echo $page->siteRoot;?>a/editlink">Edit a Link Resource</a></li><?php  
	 ?></ul>
<?php if($session->is_admin()){?>
    <h3 style="margin-top:15px;">Wahey, you have some special options</h3>
    <ul><?php  
		 ?><li><a href="<?php echo $page->siteRoot;?>a/pages">Edit Pages</a></li><?php  
		 ?><li><a href="<?php echo $page->siteRoot;?>a/emails">Emails</a></li><?php  
		 ?><li><a href="<?php echo $page->siteRoot;?>a/stats">Site Statistics</a></li><?php  
	 ?></ul>
<?php }
 $page->foot();
}else{
 header('Location: '.$page->siteRoot);
 exit;
}
?>