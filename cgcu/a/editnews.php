<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','../');
include(ROOT.'include/common.php');

if(!$session->signed_in){
 header('Location: '.$page->siteRoot.'a/signin');
 exit;
}

if(isset($_POST['submit'])){
 $filename=$_POST['filename'];
 $title=stripslashes($_POST['title']);
 $entry=stripslashes($_POST['entry']);
 $comments=isset($_POST['comments'])?'1':'0';
 
 if(strlen($title)>65){
  $msg="<ul class=\"error\"><li>The title is too long!</li></ul>";
 }else{
 
  if($contents=@file_get_contents(ROOT.'data/news/'.$filename)){
   $contents=preg_replace("/<!--title\[(.*?)\]-->/","<!--title[$title]-->",$contents);
   $contents=preg_replace("/<!--entry_text-->(.*?)<!--end_entry_text-->/s","<!--entry_text-->$entry<!--end_entry_text-->",$contents);
   $contents=preg_replace("/<!--comments_on\[(.*?)\]-->/","<!--comments_on[$comments]-->",$contents);
   $contents.="\n<!--edited by[{$session->userinfo['user']}] datetime[".time()."]-->";
 
   $fp=fopen(ROOT.'data/news/'.$filename,'w');
   @flock($fp,LOCK_EX);
   fwrite($fp,$contents);
   @flock($fp,LOCK_UN);
   fclose($fp);
  }
  header('Location: '.$page->siteRoot.'archives/'.str_replace('.php','',$filename));
  exit;
 }
}

if(isset($_POST['delete'])){
 $filename=$_POST['filename'];
 if(file_exists(ROOT.'data/news/'.$filename))
  @unlink(ROOT.'data/news/'.$filename);
 header('Location: '.$page->siteRoot.'archives');
 exit;
}

$page->title='Administration &#187; Edit a News Item';
$page->pagetitle='Edit a News Item to the CGCU website';
$page->head();

if(isset($_POST['preview'])){
 $title=stripslashes(@$_POST['title']);
 $entry=stripslashes(@$_POST['entry']);
?>
    <h3>Previewing News Item</h3>
    <h4 class="news"><?php echo $title;?></h4>
    <p class="news">
     <span class="newsdate">Posted <?php echo date('F jS, Y \a\t H:ia',time());?></span>
     <span class="newscomments"><?php echo ($comments=='1'?'<a href="#">No comments</a>':'Comments are closed');?> - by <?php echo $session->userinfo['name'];?></span>
     <br style="clear:both;" />
    </p>
    <?php echo bbcode($entry,' class="newstext"');?> 
    <div class="hr"></div>
<?php 
}

if((!isset($_POST['submit']) || isset($msg)) && !isset($_POST['delete']) && isset($_GET['p1'])){
 $filename=$_GET['p1'].'.php';
 if($contents=@file_get_contents(ROOT.'data/news/'.$filename)){
  if(!isset($_POST['submit']) && !isset($_POST['preview'])){
   preg_match("/<!--title\[(.*?)\]-->/",$contents,$matches);
   $title=$matches[1];
 
   $entry=split("<!--entry_text-->",$contents);
   $entry=split("<!--end_entry_text-->",$entry[1]);
   $entry=rtrim(str_replace(array('<p>','</p>'),array('',"\n\n"),$entry[0]));

   preg_match("/<!--comments_on\[(.*?)\]-->/",$contents,$matches);
   $comments=$matches[1];
	}
?>
    <h3>Editing '<?php echo $title;?>'</h3>
<?php if(isset($msg)){?>
    <?php echo $msg;?>
<?php }?>
    <form action="<?php echo $page->siteRoot.'a/editnews/'.$_GET['p1'];?>" method="post" class="post">
     <input type="hidden" name="filename" value="<?php echo $filename;?>" />
     Title:<br />
     <input type="text" name="title" value="<?php echo htmlspecialchars($title);?>" class="text" /><br />
     Content:<br />
     <div id="toolbar"></div>
     <script language="JavaScript" type="text/javascript">
      <!--
      <?php 
      for($i=0;$i<count($tools);$i++){
       echo "format.tools[{$i}]=[];";
			 foreach($tools[$i] as $k=>$t) echo "format.tools[{$i}]['{$k}']='{$t}';";
			}?> 
      format.load('<?php echo $page->siteRoot;?>images/toolbar/','toolbar','entry');
      //-->
     </script>
     <textarea id="entry" name="entry" rows="15" cols="40" class="text"><?php echo htmlspecialchars($entry);?></textarea>
     <p class="more"><input type="checkbox" id="comments" name="comments"<?php echo ($comments=='1'?' checked="checked"':'');?> /><label for="comments"> allow comments (automatically closed after 90 days).</label></p>
     <p class="center">
      <input type="submit" name="preview" value="Preview" class="button" />
      <input type="submit" name="delete" value="Delete" class="button" onclick="return confirm('Are you sure you want to delete the news entry \'<?php echo $title;?>\'?');" />
      <input type="submit" name="submit" value="Save" class="button" />
      <input type="button" value="Cancel" class="button" onclick="window.location='<?php echo $page->siteRoot;?>a/editnews'" />
     </p>
    </form>
<?php  
 }else{
?>
    <h3>File cannot be found</h3>
    <ul class="error"><li>Sorry but the file you want to edit just isn't there.</li></ul>
<?php 
 }
}else{
?>
    <h3>Choose a News Item to Edit</h3>
<?php 
 $handle=opendir(ROOT.'data/news/');
 while(false!==($file=readdir($handle))){
  if($file!="." && $file!=".."){
   if(preg_match("/^\d{5}\.php$/",$file)) $files[]=$file;
  }
 }
 closedir($handle);
 if(isset($files)){
  rsort($files);
?>
    <ul><?php 
  foreach($files as $file){
   if($contents=@file_get_contents(ROOT.'data/news/'.$file)){
    preg_match('/<!--title\[(.*?)\]-->/',$contents,$title);
    preg_match('/<!--datetime\[(.*?)\]-->/',$contents,$date);
    preg_match('/<!--author\[(.*?)\]-->/',$contents,$author);
    ?><li><a href="<?php echo $page->siteRoot;?>a/editnews/<?php echo str_replace('.php','',$file);?>"><?php echo $title[1].' - '.$author[1];?></a> (<?php echo date('d/m/Y @ H:i:s',$date[1]);?>)</li><?php 
   }
  }
  ?></ul><?php 
 }else{?>
    <ul class="error"><li>There is no CGCU news, how boring!</li></ul>
<?php }
}
toolbar_popups(); 
$page->foot();
?>