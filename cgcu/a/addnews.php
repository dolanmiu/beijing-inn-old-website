<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','../');
include(ROOT.'include/common.php');

if(!$session->signed_in){
 header('Location: '.$page->siteRoot.'a/signin');
 exit;
}

if(isset($_POST['submit'])){
 $entry=stripslashes(@$_POST['entry']);
 $title=stripslashes(@$_POST['title']);
 $comments=isset($_POST['comments'])?'1':'0';

 if(strlen($title)>65){
  $msg="<ul class=\"error\"><li>The title is too long!</li></ul>";
 }else{

  $handle=opendir(ROOT.'data/news/');
  while(false!==($file=readdir($handle))){
   if($file!="." && $file!=".." && preg_match("/^\d{5}\.php$/",$file)) 
    $filename[]=$file;
  }
  closedir($handle);

  if(isset($filename)){
   list($num,$ext)=split("\.",$filename[count($filename)-1]);
   $num=str_pad($num+1,5,'0',STR_PAD_LEFT);
  }else{
   $num='00001';
  }
  $blog="<!--datetime[".time()."]-->\n<!--title[$title]-->\n<!--author[{$session->userinfo['name']}]-->\n<!--filenum[$num]-->\n<!--entry_text-->$entry<!--end_entry_text-->\n<!--comments_on[$comments]-->\n<!--comments_start-->\n<!--comments_end-->";
 
  $fp=fopen(ROOT.'data/news/'.$num.'.php','w');
  @flock($fp,LOCK_EX);
  fwrite($fp,$blog);
  @flock($fp,LOCK_UN);
  fclose($fp);
 
  $fp=fopen(ROOT.'data/news/'.$session->username.'-saved.txt','w');
  @flock($fp,LOCK_EX);
  fwrite($fp,'');
  @flock($fp,LOCK_UN);
  fclose($fp);
 
  header('Location: '.$page->siteRoot.'archives/'.$num);
 }
}

if(isset($_POST['save'])){
 $title=@$_POST['title'];
 $entry=@$_POST['entry'];
 $comments=isset($_POST['comments'])?'1':'0';

 $saved="{$comments}[--#separator#--]{$title}[--#separator#--]{$entry}";
 $fp=fopen(ROOT.'data/news/'.$session->username.'-saved.txt','w');
 @flock($fp,LOCK_EX);
 fwrite($fp,$saved);
 @flock($fp,LOCK_UN);
 fclose($fp);

 $msg='<ul class="success"><li>The entry has been saved.</li></ul>';
}

if($contents=@file_get_contents(ROOT.'data/news/'.$session->username.'-saved.txt')){
 if(preg_match("/\[--#separator#--\]/",$contents)){
  list($comments,$title,$entry)=split("\[--#separator#--\]",$contents);
  $title=stripslashes($title);
  $entry=stripslashes($entry);
 }
}elseif(!isset($_POST['submit'])){
 $title='';
 $entry='';
 $comments='1';
}

if(isset($_POST['title'])){
 $title=@$_POST['title'];
 $entry=@$_POST['entry'];
 $comments=isset($_POST['comments'])?'1':'0';
}

$page->title='Administration &#187; Add News Item';
$page->pagetitle='Add a News Item to the CGCU website';
$page->head();

if(isset($_POST['preview'])){
?>
    <h3>Previewing News Item</h3>
    <h4 class="news"><?php echo stripslashes(@$_POST['title']);?></h4>
    <p class="news">
     <span class="newsdate">Posted <?php echo date('F jS, Y \a\t H:ia',time());?></span>
     <span class="newscomments"><?php echo ($comments=='1'?'<a href="#">No comments</a>':'Comments are closed');?> - by <?php echo $session->userinfo['name'];?></span>
     <br style="clear:both;" />
    </p>
    <?php echo bbcode(stripslashes(@$_POST['entry']),' class="newstext"');?> 
    <div class="hr"></div>
<?php 
}
?>
    <h3>Add a News Item</h3>
<?php if(isset($msg)){?>
    <?php echo $msg;?> 
<?php }?>
    <form action="<?php echo $page->siteRoot.'a/addnews';?>" method="post" class="post">
     <h4>Title:</h4>
     <input type="text" name="title" value="<?php echo htmlspecialchars($title);?>" class="text" /><br />
     <h4>Content:</h4>
     <div id="toolbar"></div>
     <script type="text/javascript">
      <!--
      <?php 
      for($i=0;$i<count($tools);$i++){
       echo "format.tools[{$i}]=[];";
			 foreach($tools[$i] as $k=>$t) echo "format.tools[{$i}]['{$k}']='{$t}';";
			}?> 
      format.load('<?php echo $page->siteRoot;?>images/toolbar/','toolbar','entry');
      //-->
     </script>
     <textarea id="entry" name="entry" rows="15" cols="40" class="text"><?php echo htmlspecialchars($entry);?></textarea>
     <p class="more"><input type="checkbox" id="comments" name="comments"<?php echo ($comments=='1'?' checked="checked"':'');?> /><label for="comments"> allow comments (automatically closed after 90 days).</label></p>
     <p class="center">
      <input type="submit" name="preview" value="Preview" class="button" />
      <input type="submit" name="save" value="Save" class="button" />
      <input type="submit" name="submit" value="Publish" class="button" />
      <input type="button" value="Cancel" class="button" onclick="window.location='<?php echo $page->siteRoot;?>a/'" />
     </p>
    </form>
<?php 
toolbar_popups(); 
$page->foot();
?>