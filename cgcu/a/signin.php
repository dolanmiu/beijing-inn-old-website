<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','../');
include(ROOT.'include/common.php');

if($session->signed_in){
 if(isset($_GET['r']) && $_GET['r']!='')
  header('Location: '.$_GET['r']);
 else
  header('Location: '.$page->siteRoot);
 exit;
}

if(isset($_POST['submit']) || isset($_GET['autosign'])){
 if($session->signin($_POST['user'],$_POST['pass'],$error)){
  if(isset($_POST['r']) && $_POST['r']!='')
   header('Location: '.$_POST['r']);
  else
   header('Location: '.$page->siteRoot.'a/');
 }	
}
$_POST['user']=isset($_POST['user'])?$_POST['user']:(isset($_COOKIE['user'])?$_COOKIE['user']:'');

$page->title='Administration &#187; Sign in';
$page->pagetitle='Sign in to the CGCU website';
$page->scripts=array('$(function(){',' if(document.forms[0])','  document.forms[0].'.($_POST['user']!=''?'pass':'user').'.focus();','});');

$page->head();
?>
    <h3>Sign in</h3>
    <form action="<?php echo $page->siteRoot;?>a/signin" method="post" id="signin">
<?php if(count($error)>0){?>
     <ul class="error"><li><?php echo implode('</li><li>',$error);?></li></ul>
<?php }?>
     <div id="_signin">
      <table border="0">
       <tr>
        <th<?php echo ($error['user']?' class="text_error"':'');?>>Username:</th><td><input type="text" id="user" name="user" class="text<?php echo ($error['user']?' input_error':'');?>" value="<?php echo field($_POST['user']);?>" tabindex="1" /></td>
        <td rowspan="2" style="width:200px;text-align:center;"><input type="submit" id="submit" class="button" name="submit" value="Sign in &#187;" /></td>
       </tr>
       <tr><th<?php echo ($error['pass']?' class="text_error"':'');?>>Password:</th><td><input type="password" id="pass" name="pass" class="text<?php echo ($error['pass']?' input_error':'');?>" tabindex="2" /></td></tr>
      </table>
      <input type="hidden" name="r" value="<?php echo field((isset($_GET['r'])?$_GET['r']:$_POST['r']));?>" />
     </div>
    </form>
    <div class="hr"></div>
<?php 
$page->foot();
?>