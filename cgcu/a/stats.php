<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','../');
include(ROOT.'include/common.php');

if(!$session->signed_in || !$session->is_admin()){
 header('Location: '.$page->siteRoot.'a/signin');
 exit;
}

$page->title='Administration &#187; Statistics';
$page->pagetitle='CGCU website statistics';
$page->head();
?>
    <h3>Statistics</h3>
    <ul><?php
foreach($session->stats['online'] as $k=>$v){
 list($vu)=explode(' ',$v['url']);
 echo "<li><strong>$k</strong> visited <strong><a href=\"{$vu}\">{$v['url']}</a></strong> at <strong>".date('H:i:s d/m/Y',$v['time']).'</strong></li>';
}
?></ul>
<?php  
$page->foot();
?>