<?php 
function fatal_error($message='Fatal image error',$size=''){
 $size=(int)preg_replace("/[^0-9]/",'',$size);
 if($size=='') $size=200;
 if($im=@imagecreate($size,$size)){
  if(isset($_GET['bg_image']) && is_file($_GET['bg_image'])){
   $ext_bgimg=pathinfo($_GET['bg_image']);
   $ext_bgimg=strtolower($ext_bgimg['extension']);

   switch($ext_bgimg){
    case 'gif' : $bg_image=@imagecreatefromgif($_GET['bg_image']);break;
    case 'jpeg': case 'jpg' : $bg_image=@imagecreatefromjpeg($_GET['bg_image']);break;
    case 'png' : $bg_image=@imagecreatefrompng($_GET['bg_image']);break;
   }
   imagecopy($im,$bg_image,0,0,0,0,$size,$size);
  }else{
   $background_color=imagecolorallocate($im,224,224,224);
  }
  $text_color=imagecolorallocate($im,113,113,113);
	$font=2;
  $font_width=imagefontwidth($font);
  $font_height=imagefontheight($font);
  $lines=explode("\n",wordwrap($message,floor(($size-10)/$font_width),"\n",1));
	$y=($size-(count($lines)*$font_height))/2;
  while(list($numl,$line)=each($lines)){
   imagestring($im,$font,($size-(strlen($line)*$font_width))/2,$y,$line,$text_color);
   $y+=$font_height;
  }
  header("Content-type: image/png");
  imagepng($im);
  imagedestroy($im);
  exit;
 }
 header("HTTP/1.0 500 Internal Server Error");
 print($message);
 exit;
}

if(!isset($_GET['file']) || !isset($_GET['size']))
 fatal_error('Image variables not specified correctly!');
else{
 $file=$_GET['file'];
 $size=$_GET['size'];
}
$ext=pathinfo($file);
$ext=strtolower($ext['extension']);

$cached='./cache/'.md5($file.$size).'.'.$ext;
if(is_file($cached)){
 switch($ext){
  case 'gif' : $cached_image=@imagecreatefromgif($cached);                           
               header('Content-type: image/gif');
               @imagegif($cached_image);break;
  case 'jpeg':
  case 'jpg' : $cached_image=@imagecreatefromjpeg($cached);                           
               header('Content-type: image/jpeg');
               @imagejpeg($cached_image,null,100);break;
  case 'png' : $cached_image=@imagecreatefrompng($cached);                           
               header('Content-type: image/png');
               @imagepng($cached_image);break;
 }
}

if(!is_file($file))
 fatal_error('Oh no, the image is not here..',$_GET['size']);

if(!list($old_width,$old_height)=@getimagesize($file)){
 fatal_error('Error in finding image dimensions!',$_GET['size']);
}
$ratio=$old_width/$old_height;

if(substr($size,0,1)=='h') $type='fixedheight';
elseif(substr($size,0,1)=='w') $type='fixedwidth';
elseif(substr($size,0,1)=='m') $type='max';
elseif(substr($size,0,1)=='c') $type='crop';
elseif(substr($size,0,1)=='s') $type='square';
elseif($old_height>$old_width) $type='fixedheight';
else $type='fixedwidth';

$size=(int)preg_replace('/[^0-9]/','',$size);
if(!$size) $size=200;
$dst_x=$dst_y=0;
$src_x=$src_y=0;

if($type=='max'){
 if($old_height>$old_width){
  if($old_height>$size){
   $img_width=$new_width=floor($size*$ratio);
   $new_height=$img_height=$size;
	}else{
   $img_width=$new_width=$old_width;
   $img_height=$new_height=$old_height;
	}
 }else{
  if($old_width>$size){
   $img_width=$new_width=$size;
   $new_height=$img_height=floor($size/$ratio);
	}else{
   $img_width=$new_width=$old_width;
   $img_height=$new_height=$old_height;
	}
 }
}elseif($type=='square'){
 $img_width=$new_width=$size;
 $img_height=$new_height=$size;
}elseif($type=='crop'){
 if($old_height>$size){
  $img_height=$new_height=$old_height=$size;
 }else{
  $img_height=$new_height=$old_height;
 }
 if($old_width>$size){
  $img_width=$new_width=$old_width=$size;
 }else{
  $img_width=$new_width=$old_width;
 }
}elseif($type=='fixedheight'){
 $img_height=$size;
 if($size>$old_height){
  $dst_y=($size-$old_height)/2;
  $new_width=$img_width=floor($old_height*$ratio);
  $new_height=$old_height;
 }else{
  $new_width=$img_width=floor($size*$ratio);
  $new_height=$size;
 }
}else{
 $img_width=$size;
 if($size>$old_width){
  $dst_x=($size-$old_width)/2;
  $new_width=$old_width;
  $new_height=$img_height=floor($old_width/$ratio);
 }else{
  $new_width=$size;
  $new_height=$img_height=floor($size/$ratio);
 }
}

if(!function_exists('imagecreatetruecolor'))
 echo 'GD library not available.';
elseif(!$new_image=@imagecreatetruecolor($img_width,$img_height))
 fatal_error('Error in creating image!');
 
$bg_col=@imagecolorallocate($new_image,255,255,255);
@imagefill($new_image,0,0,$bg_col);
 
if(isset($_GET['bg_image']) && is_file($_GET['bg_image'])){
 $ext_bgimg=pathinfo($_GET['bg_image']);
 $ext_bgimg=strtolower($ext_bgimg['extension']);

 switch($ext_bgimg){
  case 'gif' : $bg_image=@imagecreatefromgif($_GET['bg_image']);break;
  case 'jpeg': case 'jpg' : $bg_image=@imagecreatefromjpeg($_GET['bg_image']);break;
  case 'png' : $bg_image=@imagecreatefrompng($_GET['bg_image']);break;
 }
 imagecopy($new_image,$bg_image,0,0,0,0,$img_width,$img_height);
}

switch($ext){
 case 'gif' : $old_image=@imagecreatefromgif($file);                           
              @imagecopyresampled($new_image,$old_image,$dst_x,$dst_y,$src_x,$src_y,$new_width,$new_height,$old_width,$old_height);
							@imagedestroy($old_image);
							header('Content-type: image/gif');
              @imagegif($new_image,$cached);
              @imagegif($new_image);break;
 case 'jpeg':
 case 'jpg' : $old_image=@imagecreatefromjpeg($file);                           
              @imagecopyresampled($new_image,$old_image,$dst_x,$dst_y,$src_x,$src_y,$new_width,$new_height,$old_width,$old_height);
							@imagedestroy($old_image);
							header('Content-type: image/jpeg');
              @imagejpeg($new_image,$cached,100);
              @imagejpeg($new_image,null,100);break;
 case 'png' : $old_image=@imagecreatefrompng($file);                           
              @imagecopyresampled($new_image,$old_image,$dst_x,$dst_y,$src_x,$src_y,$new_width,$new_height,$old_width,$old_height);
							@imagedestroy($old_image);
              header('Content-type: image/png');
              @imagepng($new_image,$cached);
              @imagepng($new_image);break;
}
imagedestroy($new_image);
exit();
?>
