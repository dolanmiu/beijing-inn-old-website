<?php
	ob_start();
	echo '<?xml version="1.0" encoding="UTF-8"?>'
?>
<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','./');
include(ROOT.'include/common.php');
?>


<rss version="2.0">
  <channel>
    <title>City &amp; Guilds College Union Events</title>
    <link>http://www.cgcu.net/</link>
    <copyright>(c)City &amp; Guilds College Union</copyright>
    <language>en-gb</language>

    <lastBuildDate><?php echo date('r', time());?></lastBuildDate>
    <description>Events coming up by City &amp; Guilds College Union</description>

<?php

 foreach($data_events as $event){
	list($d,$m,$y)=explode('/',$event['date']);
	list($h,$i)=explode(':',$event['time']);
	$etime=mktime($h,$i,0,$m,$d,$y);
	if($etime>(time()-60*60*48)) $_nevents[$etime]=$event;
	else $_oevents[$etime]=$event;
 }
 isset($_nevents)?ksort($_nevents):null;
 isset($_oevents)?krsort($_oevents):null;

?> 
<?php
  if(count($_nevents)) {
	  foreach($_nevents as $n=>$event) show_event($event,$n<count($_nevents)-1);
  }
/*  if(count($_oevents)) {
	  foreach($_oevents as $n=>$event) show_event($event,$n<count($_oevents)-1);
  }*/

 

function show_event($event,$border){
 global $page,$pun_user;
	list($d,$m,$y)=explode('/',$event['date']);
	list($h,$i)=explode(':',$event['time']);
	$time=mktime($h,$i,0,$m,$d,$y);
?>
    <item>
      <title><?php echo strip_crap($event['title']);?></title>
      <description>
		<?php echo date('jS F, Y',$time).' @ '.date('H:ia',$time);?> - <?php echo $event['whereto'];?>
	</description>
      <link>http://www.cgcu.net<?php echo $page->siteRoot;?>events/<?php echo url_maker($event['title'],$event['date']);?></link>
      <pubDate><?php echo date('r',$time);?></pubDate>
      <guid isPermaLink="true">http://www.cgcu.net<?php echo $page->siteRoot;?>events/<?php echo url_maker($event['title'],$event['date']);?></guid>
    </item>

<?php 
}
?>
</channel>
</rss>

<?php ob_end_flush(); ?>
