<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','./');
include(ROOT.'include/common.php');

if(isset($_GET['p'])&&!empty($_GET['p'])&&in_array($_GET['p'],array_keys($data_pages['about']))){
 $about=$_GET['p'];
 if(!$data=@file_get_contents(ROOT.'data/pages/about-'.$about.'.php')){
  header('Location: '.$page->siteRoot.'404-error');
	exit;
 }
 $page->title='About the CGCU &#187; '.$data_pages['about'][$about];
 $page->pagetitle='About the CGCU';
}else{
 $about='about';
 $data=@file_get_contents(ROOT.'data/pages/about.php'); 
 $page->title='About the CGCU';
 $page->pagetitle='Overview of the CGCU';
}

$page->head();
echo bbcode($data);
$page->foot();
?>