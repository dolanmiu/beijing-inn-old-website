<?php 
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
$data_pages=array(
 'about'=>array(
  'committees'=>'Committees',
  'officers'=>'Officers',
  'alumni'=>'Alumni',
  'honour-shields'=>'Honour Shields',
  'history'=>'History'
 )
);

?>