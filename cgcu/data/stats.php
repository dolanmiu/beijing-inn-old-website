<?php
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk
 * @date:     May 2008
 *
 * Please do not change anything
 * unless you know what you're doing!
 *
*/
if(!defined('BEGIN')){
 die('Die: Hacking attempt');
}

$stats=array (
  'online' => 
  array (
    '82.26.18.151' => 
    array (
      'time' => 1317591644,
      'url' => '/ (/cgcu/index.php)',
    ),
  ),
  'most_online' => 11,
);

?>