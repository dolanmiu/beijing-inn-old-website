<?php 
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
$data_clubs=array(
 'deps'=>array(
  'AeroSoc'=>array('dep'=>'Aeronautical Engineering','site'=>'http://www.cgcu.net/aero/','chair'=>'Tom Murray','img'=>'aerosoc.png','info'=>'AeroSoc is the society for Aeronautical Engineering students at Imperial College London - if you\'re part of Aero, you\'re part of AeroSoc! '),
  'BioEngSoc'=>array('dep'=>'Bioengineering','site'=>'http://www.cgcu.net/bioeng/','chair'=>'Clinton Goh','img'=>'bioeng.png','info'=>'The Bioengineering Society was founded about 10 years ago by enthusiastic MSc students in the Department with the aim of broadening the experiences of Bioengineering beyond that in the curriculum, to forge links with potential future employers and to organise social activities. Today, the society organizes a range of social events throughout the academic year, to provide fun and well deserved opportunities for hardworking Bioengineers to take a break from their books.'),
  'ChemEngSoc'=>array('dep'=>'Chemical Engineering','site'=>'http://www.cgcu.net/chemeng/','chair'=>'','img'=>'','info'=>''),
  'CivSoc'=>array('dep'=>'Civil Engineering','site'=>'http://www.cgcu.net/civsoc/','chair'=>'','img'=>'civsoc.jpg','info'=>'CivSoc is the society for students in thedepartment of Civil and Environmental Engineering, all of whom are automatically members free of charge.

We are one of the most active departmental societies in the college, organising a wide variety of events throughout the year to cater for the diverse needs and interests of our students. The annual international tour, the highlight of CivSoc\'s calendar, gives students the chance to explore contemporary and traditional engineering practice outside the UK as well as sampling the social and cultural delights of some of Europe\'s best loved cities. Destinations in recent years have included Copenhagen,Budapest, Barcelona, Berlin and Athens.

Throughout the year, regular social events, pub-crawls and parties are complemented by site visits and professional lectures; the latter aided by our strong links with the civil engineering industry. The society\'s departmental magazine, Livic, is published monthly.'),
  'DoCSoc'=>array('dep'=>'Department of Computing','site'=>'http://www.cgcu.net/docsoc/','chair'=>'','img'=>'','info'=>''),
  'EESoc'=>array('dep'=>'Electrical and Electronic Engineering','site'=>'http://www.cgcu.net/eesoc/','chair'=>'James Yip','img'=>'eesoc.png','info'=>''),
  'MatSoc'=>array('dep'=>'Materials','site'=>'http://www.union.ic.ac.uk/rsm/matsoc.php','chair'=>'','img'=>'','info'=>''),
  'MechSoc'=>array('dep'=>'Mechanical Engineering','site'=>'http://www.cgcu.net/mechsoc/','chair'=>'Vishal Shah','img'=>'mechsoc.png','info'=>'MechSoc is the Imperial College Mechanical Engineering Society, and before you ask all Mechanical Engineering students are automatically members! MechSoc is here to help students enjoy themselves outside of the stress of lectures, coursework and all the other things going on in the department. In addition to our great parties, we organise plenty of other events such as visits from top companies, industrial presentations from our sponsors and much more!')
 )
);

?>