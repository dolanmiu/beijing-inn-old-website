[h]Executive Committee[/h]
[ul]
 [*][b]Chair[/b] Holly Farrer
 [*][b]President[/b] He-in Cheong
 [*][b]Vice President (Activities)[/b] Kesh Murthy
 [*][b]Vice President (Finance & Societies)[/b] Jacky (Tsz) Kwan
 [*][b]Honorary Secretary[/b] Faizan Khan
 [*][b]Academic Affairs Officer (Taught)[/b] Marcus Ulmefors
 [*][b]Welfare Officer[/b] Sahil Chugani
 [*][b]CGCA Officer[/b] Lawrence Weetman
 [*][b]Sports Officer[/b] Jess Poore
 [*][b]RAG Coordinator[/b] To Be Elected
 [*][b]Bo' Driver[/b] Robert Carter
 [*][b]Spanner Bearer[/b] Matthew Wright
 [*][b]Bolt Bearer[/b] Andrew Smith
[/ul]

[h]CGCU General Meetings[/h]
The City and Guilds College Union is ultimately governed by General Meetings, which are usually held once each term. All 4,200 members of the Union are eligible to vote at General Meetings, which is why they are an excellent way of enabling our members to hold their officers to account and stand for election to some positions.

Our Annual General Meeting is usually held towards the end of the Easter Term. At this meeting the annual report is received, the general election results are announced and awards are given out to those who have made a substantial contribution to CGCU.

[h]CGCU Executive[/h]
The day-to-day management and policy making body of the Union is it's Executive Committee, which meets every three weeks during term time. All Senior Central Union Officers, Departmental Representatives and Departmental Society Chairs sit on the Executive Committee, as well as the Royal School of Mines President, Sports Officer, CGCA Officer and an Ordinary Member elected from the Clubs Activities and Committee.

[h]CGCU Management Committee[/h]
Further day-to-day Central Union Management is delegated to the Management Committee, which is formed of Senior Union Officers and two Ordinary Members who are elected from the Executive. This committee is also responsible for developing the Union's strategy and policy, subject to the approval of the Executive.

[h]Departmental Society Committees[/h]
We manage seven Departmental Societies, which are responsible for managing social activities within each of the Faculty of Engineering's departments. In general these highly successful societies co-ordinate large events, arrange presentations, publish newsletters, manage websites and in some cases organise tours and excursions. Additionally, the Royal School of Mines Clubs and Societies Committee govern two other Faculty of Engineering societies.

[h]CGCU Events Committee[/h]
The events committee is responsible for co-ordinating traditional and innovative central Union events such as the Lord Mayor's Show submission, the Egg Race and various RAG Week events. Its membership is drawn from the students who fulfil traditional roles within the Union and are usually elected through General Meetings.

[h]Student Affairs Committee[/h]
We also operate two student affairs committees that deal with academic, welfare and representation issues. There is one committee for taught students chaired by the Academic Affairs Officer (Taught) and a second for research students chaired by the Academic Affairs Officer (Research). All Departmental Representatives attend TSAC (the Taught Student Affairs Committee) and all Research Representatives attend RSAC (the Research Student Affairs Committee). The President, Vice President and Honorary Secretary sit both committees.