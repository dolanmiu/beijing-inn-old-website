Imperial College of Science and Technology was formed in 1907 from a merger between the Royal College of Science, the City and Guilds College, and the Royal School of Mines.

Traditionally, crafts and trades in the city were governed by London Livery Companies. These were a combination of guilds and workers' unions, which regulated trade standards as well as workers' wages, rights and working standards. In 1876 there were only seventeen, but today there
are over 100, which have evolved to control more modern professions and trades (the most recent being the Worshipful Company of Tax Advisers).

In 1865, when the original 17 companies met, they moved to found a new institution, the City and Guilds of London Institute, which existed to improve training of craftsmen and to set up a  standardised system of qualification examinations. This institute began its work in 1879, and created two subsidiary colleges before being incorporated into Imperial College.

The City and Guilds Central Technical College, later just the City and Guilds College, continued to be a constituent of Imperial until 2002, when it and the Royal School of Mines became the Faculty of Engineering.

The union of students of the CGC was always known as the City and Guilds Union, and when the Faculty was formed continued as the body charged with looking after the welfare of the students, as well as the students of the Royal School of Mines. We also try to uphold the history and traditions
of the College, run several engineering-based societies, and keep in close touch with the Engineering Alumni of Imperial.