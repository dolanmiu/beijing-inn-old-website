[h]Our Aims &amp; Objectives[/h]
[ul]
 [*]First and foremost, we represent our students to their departments, the Faculty, Imperial College Union and other bodies.
 [*]We campaign on welfare issues important to our members and co-ordinate departmental Buddies Schemes.</li>
 [*]We run Departmental Societies that arrange academic and social events relevant to the discipline of each department.</li>
 [*]We run a handful of engineering related clubs and societies, such as our rugby, media and motor clubs.</li>
 [*]We run an Internship Centre that helps engineering students find summer vacation work in a wide range of Engineering, Finance and Management companies.</li>
 [*]We co-ordinate a large number of faculty wide social and traditional events including the largest Freshers&#039; Ball at Imperial College, outrageous RAG Events and our entry in the Annual Lord Mayor&#039;s Show parade.</li>
 [*]We maintain a link with our Alumni Associations and encourage our students to involve themselves in the Alumni network and social calendar.</li>
[/ul]

The City and Guilds College Union (CGCU) is the student voice of the Faculty of Engineering and the Tanaka Business School at Imperial College in London. We are part of the Imperial College Union, which represents all 12,000 students of Imperial College. Our students range from a far a breadth as Computing and Electronics to Geology and Health Management. However, all these programmes of study have one aspect in common - they all hone the essential skills, competence and attributes that make our students different from Scientific and Medical students.

The role we play in the life of our members is one that is consistently adapting to what they want. We provide essential services that not only extend the experience of college beyond the academic side, but also enhance and support it.

To our students, we remain one of the most fundamentally important aspects of life at Imperial College, partly due to the enthusiasm of our students who become involved in helping provide these services. For reasons largely historical and political, we are named City &amp; Guilds College Union, after the Constituent College that first helped form Imperial College.
