[h]President, Mark Mearing-Smith[/h]

Mark, our beloved President, is a third year Mechanical engineer. The role of President is a big one, especially for someone on a full time course; as President you don't just do the glamorous student events, there is a raft of serious meetings with the Sabbatical Officers in the central Union as well as the head honchos of the Faculty (staff).

Luckily Mark has a bit of experience behind him! Last year he was Council Chair, which as the name suggests chairing Central Union Council, but also sitting on the first Trustee Board. It doesn't stop there though, our multifaceted President not only chaired the hacks, he also was treasurer of the Snowsports club, but we forgive him for that. So you could say that he is a bit of finacial know-all. He is also Chair of the Snowsports club this year.

Now that we have clarified that our President is pretty reliable, what can he do for you, the ordinary student? Well for a start no one at Imperial is ordinary and contrary to popular ideals we are an elite university full of particularly hard working people, wherein lies the problem. All this work and stress means we need a top notch Faculty Union which runs plenty of cheap or even free events, as well as coordinating good back up support in terms of student welfare. To start the ball rolling Mark spent the summer slaving away in the office to secure vital sponsorship from companies and grant from the Faculty to help support the roster of activities over the coming year - and there are a fair few including the Masquerade Ball!

This is all well and good but a President is useless if you can't contact him! The best way to contact most people in college is via email, usually checked a minimum of twenty times a day and more if your are in a particularly boring MatLab class (don't worry if you don't know what that is, you soon will), and even more because Mark obviously have a BlackBerry. If you are feeling a little more adventurous you can also pop into the CGCU offices, room 340 Mechanical Engineering. This spacious office suite has computers, wifi, sofas and coffee machine and most of the officers if it is a lunchtime; and if not there will be someone else!

[h]Honorary Secretary, Kirsty Patterson[/h]

We finally have an Honorary Secretary, the person who is in charge of organising the committees of Guilds and the office, thankfully Kirsty stepped in to help organise that before she was even elected event. 


[h]Vice President (Activities), Matthew Taylor[/h]
Matt is our Vice President (Activities), an odd title since it is missing an "of" and activities is in brackets. The job includes running all the activities and events (minus those done by the Sports Officer) of CGCU as well as acting as deputy to the President for the more serious workings of the CGCU. It is quite a big job to take on as it also involves coordinating sponsorship and keeping in tough with all of CGCU's societies to make sure everything is running smoothly.

Thankfully Matt is an enthusiastic soul who also is the Chairman of RCS Motor (aka Jez). As another third year Mechanical Engineer it will be Matt's job to run great events such as Paintballing, Cinema Nights, the infamous "Egg Race", Bar Nights, RAG Week (which includes a barbershop quartet) and other assorted activities. Like most things in life these need money and so begins the careful task of making prices as cheap as possible for students, but not making CGCU bankrupt! Although we have numerous traditional events we are always looking to do more and capture a greater range of students in our activities.

If you have an idea about an event or even want to help organise and run one don't hesitate to contact Matt, either by email via [guilds.vpa@ic.ac.uk|guilds.vpa@ic.ac.uk] or popping in to the CGCU office at lunchtime.

[h]Vice President (Finance & Societies), John "JJ" James[/h]
JJ is our Vice President (Finance & Societies). He is the money man! He is also the main coordinator of the clubs and societies in the Guilds. 