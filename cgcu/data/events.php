<?php
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk
 * @date:     May 2008
 *
 * Please do not change anything
 * unless you know what you're doing!
 *
*/
if(!defined('BEGIN')){
 die('Die: Hacking attempt');
}

$data_events=array (
  0 => 
  array (
    'id' => 1,
    'datetime' => 1221130407,
    'author' => NULL,
    'date' => '31/10/2008',
    'time' => '19:00',
    'title' => 'Masquerade Ball',
    'whereto' => 'South Kensington Campus, Imperial College',
    'wherefrom' => '',
    'description' => 'For the first time ever the City & Guilds College Union and the Royal College of Science Union have joined forces to make possible the first Masquerade Ball at Imperial in living memory.

The occasion is a formal dinner and consists of a superb three-course meal, followed by after-dinner entertainment where we will be joined by our colleagues from the RCSU. This includes a jazz band, casino, photographer, chocolate fountain, chill-out zone and a brilliant DJ set! The dress code for the Dinner is �Masquerade� (Black Tie with a mask). With this in mind we are delighted to be able to offer you a discount on City & Guilds College Union cufflinks, when you purchase your tickets. Free masquerade masks can be collected with your ticket, or lay down the gauntlet and find your own. 

Dinner tickets cost �30 or you may if you wish purchase tickets just for the after-dinner entertainment at �15.

Tickets are for sale [url=http://www.imperialcollegeunion.org/cgcu-353/cgcu-masquerade-ball-2008-839/product.html]online[/url] from Imperial College Union or see the [url=http://www.new.facebook.com/event.php?eid=31686601959&ref=nf]facebook group[/url] for details of how to purchase by post',
    'poster' => '-1221214564.jpg',
  ),
  1 => 
  array (
    'id' => 2,
    'datetime' => 1221737932,
    'author' => NULL,
    'date' => '06/10/2008',
    'time' => '12:00',
    'title' => 'Freshers\' BBQ',
    'whereto' => 'The Queen\'s Lawn',
    'wherefrom' => '',
    'description' => 'You\'ll no doubt be wanting a quick and easy meal on your first day in college and our BBQ offers the perfect opportunity to get a cheap bite to eat.

Add on Facebook: http://www.new.facebook.com/event.php?eid=36484438407',
    'poster' => '-1221748005.jpg',
  ),
  2 => 
  array (
    'id' => 3,
    'datetime' => 1221738312,
    'author' => NULL,
    'date' => '14/10/2008',
    'time' => '19:00',
    'title' => 'City of London Pub Crawl & Treasure Hunt',
    'whereto' => 'Ctiy of London, Starting at The Bell, near Cannon St Tube',
    'wherefrom' => 'Those who can\'t navigate the tube are welcome to join is in 340 Mechanical Engineering from 18:00.',
    'description' => 'Embrace of the history of City & Guilds College Union and the Faculty of Engineering, tracing clues around the historic City of London to win the CGCU Treasure. You might like to stop at a few pubs along the way to aid with your navigation.

Teams of up to 4 people, or just do the pub crawl and have some fun. Register with Matt Taylor by emaling vpa@imperial.ac.uk.',
    'poster' => '-1221747801.jpg',
  ),
  3 => 
  array (
    'id' => 4,
    'datetime' => 1221738484,
    'author' => NULL,
    'date' => '16/10/2008',
    'time' => '20:00',
    'title' => 'Freshers\' Bar Night',
    'whereto' => 'The Union Building',
    'wherefrom' => '',
    'description' => 'The freshers\' bar night is your introduction to the Union bars, including cheap beer and a freshers\' yard competition.',
    'poster' => '',
  ),
  4 => 
  array (
    'id' => 5,
    'datetime' => 1221749253,
    'author' => NULL,
    'date' => '08/11/2008',
    'time' => '10:30',
    'title' => 'The Lord Mayor\'s Show',
    'whereto' => 'The City of London',
    'wherefrom' => 'Moorgate Tube Station, 10:00',
    'description' => 'Join City & Guilds College Union for the annual Lord Mayor\'s Show, where the new Lord Mayor of the City of London makes his way through the City to pledge allegiance to the Queen.

There\'s a reasonable chance of getting on the BBC ONE coverage!',
    'poster' => '5-1221749253.jpg',
  ),
  5 => 
  array (
    'id' => 6,
    'datetime' => 1226918945,
    'author' => NULL,
    'date' => '18/02/2009',
    'time' => '09:00',
    'title' => 'Great Egg Race',
    'whereto' => 'Imperial College London, South Kensington',
    'wherefrom' => '',
    'description' => 'Students will be asked to design and make a �flying� contraption to carry an egg safely to the ground after being launched from the 6m high balcony of the Queen�s Tower at the heart of Imperial�s South Kensington campus. The winning team will be the group which manages to get the egg the furthest distance from the tower without it breaking. The teams will be judged over three categories: the design, build quality and the eventual performance of their contraption. There will be four prizes in each age category (years10-11 and years 12-13), one for the overall winners and three for the best in each category. The event will take a whole day and will include a lecture from a leading Aeronautical Engineer, with each age group being supervised by an Imperial Lecturer. Each student team will also be ably assisted by a student mentor.

To find out more contact [url=mailto:guilds@imperial.ac.uk]guilds@imperial.ac.uk[/url]',
    'poster' => '-1226919105.JPG',
  ),
  6 => 
  array (
    'id' => 7,
    'datetime' => 1234892130,
    'author' => NULL,
    'date' => '02/03/2009',
    'time' => '23:55',
    'title' => 'Close of Nominations',
    'whereto' => 'Online: http://www.imperialcollegeunion.org/vote',
    'wherefrom' => '',
    'description' => 'Nominations Close for the Elections. 

Go and stand!!

http://www.imperialcollegeunion.org/vote',
    'poster' => '',
  ),
  7 => 
  array (
    'id' => 8,
    'datetime' => 1234892412,
    'author' => NULL,
    'date' => '06/03/2009',
    'time' => '12:30',
    'title' => 'CGCU Elections Hustings',
    'whereto' => 'Union Bar',
    'wherefrom' => '',
    'description' => 'Hustings will take place for all CGCU positions.

Free Krispy Kreams!!!',
    'poster' => '',
  ),
  8 => 
  array (
    'id' => 9,
    'datetime' => 1234963066,
    'author' => NULL,
    'date' => '18/02/2009',
    'time' => '19:00',
    'title' => 'CGCU RAG Slave Auction',
    'whereto' => 'Union Bar',
    'wherefrom' => '',
    'description' => 'Come along to put your bid in for the following confirmed slaves (and no doubt more on the day):

CGCU Preisdent : Mark Mearing-Smith
CGCU VPA : Matthew Taylor
CGCU VPFS : John James
CGCU Hon Sec : Kirsty Patterson

ICU President : Jenny Morgan
Deputy President (Finance and Services) : Christian Carter
ICSMSU Preisdent : Mark Chamberlain
Felix Editor : Jovan Nedic

RAG Chair : Jon Downing
CGCU RAG Coordinator : Alice Rowlands
Mech Eng Senior Tutor : Dr Shaun Crofton',
    'poster' => '',
  ),
  9 => 
  array (
    'id' => 10,
    'datetime' => 1235146290,
    'author' => NULL,
    'date' => '02/03/2009',
    'time' => '23:55',
    'title' => 'Last Day of Nominations Open',
    'whereto' => 'Online www.cgcu.net/vote',
    'wherefrom' => 'A Computer',
    'description' => 'This will be the last day of nominations.

Go to http://www.cgcu.net/vote 

or

http://www.imperialcollegeunion.org/vote',
    'poster' => '-1235148120.jpg',
  ),
  10 => 
  array (
    'id' => 11,
    'datetime' => 1251025912,
    'author' => NULL,
    'date' => '23/10/2009',
    'time' => '18:00',
    'title' => 'Masquerade Ball 2009',
    'whereto' => 'Imperial College Union',
    'wherefrom' => '',
    'description' => 'The Ball to end all Balls! After the success of last year the Masquerade returns bigger and better than ever. We\'ve gone all out on the entertainments this year, getting in fantastic new-wave London electro-punk-roack band "New Young Pony Club". What\'s more, this is their only university gig planned and their only dates booked in London - a real exclusive!

Click on the following link to buy tickets. You will need to login at the top of the page using your College ID and Password (eg. abc09):

[url="http://www.imperialcollegeunion.org/cgcu-353/cgcu-masquerade-ball-2008-839/product.html"]Buy tickets from the shop[/url]',
    'poster' => '11-1251025912.jpg',
  ),
  11 => 
  array (
    'id' => 12,
    'datetime' => 1252950682,
    'author' => NULL,
    'date' => '05/10/2009',
    'time' => '12:00',
    'title' => 'Freshers\' Barbeque featuring ICU Jazz Big Band',
    'whereto' => 'Beit Quad',
    'wherefrom' => '',
    'description' => 'The annual freshers\' barbeque will be served in Beit Quad from 12:00noon until 10:30pm with engineering students enjoying a whopping �2 discount off normal prices. All you have to do is show up and prove you are an engineer. Don\'t worry if you don\'t have your swipecard. We\'ll have someone on hand with a laptop so just tell them your name!

Meet the commitee, enjoy the Big Band and we\'ll even have some fun and games such as conkers and giant jenga!',
    'poster' => '-1252953995.jpg',
  ),
  12 => 
  array (
    'id' => 13,
    'datetime' => 1255334051,
    'author' => NULL,
    'date' => '19/10/2009',
    'time' => '17:30',
    'title' => 'IMechE Presentation',
    'whereto' => 'G14, Sir Alexander Fleming Building',
    'wherefrom' => '',
    'description' => 'This talk will explore the tram-train concept that has evolved in continental Europe and look at how the rail industry might benefit from of this emerging technology in the UK.

The tram-train concept, that of inter-running vehicles that are designed for use on street tram systems and on the mainline railway, offers a significant opportunity for passenger market development in the UK. The concept, however, brings with it some significant system challenges.

The presentation will be delivered by Giles Thomas, a Chartered Engineer who has responsibility for optimising the interface between future rolling stock procurements and Network Rail�s infrastructure for suburban and urban services.',
    'poster' => '',
  ),
  13 => 
  array (
    'id' => 14,
    'datetime' => 1290801004,
    'author' => NULL,
    'date' => '06/12/2010',
    'time' => '12:00',
    'title' => 'Festive Fortnight',
    'whereto' => 'London',
    'wherefrom' => '',
    'description' => 'The RCSU and C&G Union are getting together for the Christmas spirit to bring you all sorts of christmasness.

Check this page regularly to know whats happening.',
    'poster' => '',
  ),
);

?>