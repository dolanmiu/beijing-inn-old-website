<?php
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk
 * @date:     May 2008
 *
 * Please do not change anything
 * unless you know what you're doing!
 *
*/
if(!defined('BEGIN')){
 die('Die: Hacking attempt');
}

$data_links=array (
  0 => 
  array (
    'id' => 1,
    'datetime' => 1291986815,
    'author' => 'foepres',
    'url' => 'https://docs.google.com/viewer?a=v&pid=explorer&chrome=true&srcid=0B2VxXjTo_5-VY2U5MTJiZWYtZWUxYi00MTY5LThkN2ItMzY4MjVmNWI5OGEx&hl=en_GB&authkey=CNDG1KkE',
    'text' => 'Faculty Representation Committee of the City & Guilds College Union',
    'description' => 'The meeting was held in Room 340, Mechanical Engineering Building, at 17.00 on Thursday 18th November with Marcus Ulmefors in the Chair and Joseph Haley taking minutes.',
    'clicks' => 643,
  ),
);

?>