<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','./');
include(ROOT.'include/common.php');

$page->title='Archives';
$page->pagetitle='CGCU News &amp; Posts Archives';

if(isset($_GET['p'])){
 $page->pagetitle='CGCU News &amp; Posts Archives';
 $page->head();

 if($contents=@file_get_contents(ROOT.'data/news/'.$_GET['p'].'.php')){
  preg_match('/<!--title\[(.*?)\]-->/',$contents,$title);
  preg_match('/<!--datetime\[(.*?)\]-->/',$contents,$date);
  preg_match('/<!--author\[(.*?)\]-->/',$contents,$author);
  preg_match('/<!--filenum\[(.*?)\]-->/',$contents,$filenum);
  preg_match('/<!--entry_text-->(.*?)<!--end_entry_text-->/s',$contents,$text);
  preg_match('/<!--comments_on\[(.*?)\]-->/',$contents,$comments_on);
  preg_match_all('/<!--comment_start--><!--comment_allowed\[1\]-->(.*?)<!--comment_end-->/s',$contents,$comments);
  preg_match_all('/<!--comment_start--><!--comment_allowed\[(?:.*?)\]--><!--comment_id\[(.*?)\]-->(?:.*?)<!--comment_end-->/s',$contents,$comment_last);
	$comment_last=$comment_last[1][count($comment_last[1])-1];
?>
    <h3><a href="<?php echo $page->siteRoot;?>archives/<?php echo $filenum[1];?>"><?php echo $title[1];?></a></h3>
    <p class="news">
     <span class="newsdate">Posted <?php echo date('F jS, Y \a\t H:ia',$date[1]);?></span>
     <span class="newscomments"><?php $nc=count($comments[1]);echo ($comments_on[1]=='1'?'<a href="'.$page->siteRoot.'archives/'.$filenum[1].'#comments">'.($nc>0?$nc:'No').' comment'.($nc==1?'':'s').'</a>':'Comments are closed');?> - by <?php echo $author[1];?></span>
     <br style="clear:both;" />
    </p>
<?php if($session->signed_in){?>
    <p style="margin:10px 0;"><a href="<?php echo $page->siteRoot.'a/editnews/'.$filenum[1];?>" class="edit">Edit item</a></p>
<?php }?>
    <?php echo bbcode($text[1],' class="newstext"');?> 
    <p class="more"><a href="<?php echo $page->siteRoot;?>archives">View the news archive</a></p>
<?php if($session->signed_in){?>
    <p style="margin:10px 0;"><a href="<?php echo $page->siteRoot.'a/editnews/'.$filenum[1];?>" class="edit">Edit item</a></p>
<?php }
  if($comments_on[1]=='1'){
?>
    <h3 id="comments"><?php $nc=count($comments[1]);echo ($nc>0?$nc:'No').' comment'.($nc==1?' has':'s have');?> been made</h3>
<?php 
   foreach($comments[1] as $comment){
    preg_match('/<!--comment_id\[(.*?)\]-->/',$comment,$cm_id);
    preg_match('/<!--comment_datetime\[(.*?)\]-->/',$comment,$cm_date);
    preg_match('/<!--comment_name\[(.*?)\]-->/',$comment,$cm_name);
    preg_match('/<!--comment_comment\[(.*?)\]-->/',$comment,$cm_message);
?>
    <div id="comment<?php echo $cm_id[1];?>" class="comment<?php echo (($c++)%2==0?' dark':'');?>">
     <span><?php echo $cm_name[1];?> (<?php echo ago($cm_date[1]);?> ago)</span>
     <?php echo bbcode($cm_message[1]);?> 
<?php if($session->signed_in){?>
     <p style="padding-top:5px;"><a href="<?php echo $page->siteRoot.'a/newscomments?filename='.$_GET['p'].'.php&amp;comment='.$cm_id[1].'&amp;action=delete';?>" class="edit">Delete comment</a></p>
<?php }?>
    </div>
<?php 
	 }
   if(time()<(int)$date[1]+(90*24*60*60)){
    if($_POST['addcomment']==$_SESSION['addcomment_code'] && !empty($_SESSION['addcomment_code'])){
     $name=trim(stripslashes(strip_tags($_POST['name'])));
     $email=trim(stripslashes(strip_tags($_POST['email'])));
     $message=preg_replace("/[\t\r\n]/",'',nl2br(trim(stripslashes(strip_tags($_POST['message'])))));
     if(empty($name) || empty($email) || empty($message)){
?>
    <div class="hr"></div>
    <ul class="error"><li>You need to fill in all the fields!</li></ul>
<?php 
		 }else{
      $comment=" <!--comment_start--><!--comment_allowed[0]--><!--comment_id[".((int)$comment_last+1)."]--><!--comment_datetime[".time()."]--><!--comment_name[$name]--><!--comment_email[$email]--><!--comment_comment[$message]--><!--comment_ip[".$_SERVER['REMOTE_ADDR']."]--><!--comment_end-->\n";
      $contents=str_replace('<!--comments_end-->',$comment.'<!--comments_end-->',$contents);
      $fp=fopen(ROOT.'data/news/'.$_GET['p'].'.php','w');
      @flock($fp,LOCK_EX);
      fwrite($fp,$contents);
      @flock($fp,LOCK_UN);
      fclose($fp);
			$hideform=true;
      $body='<html><body><font face="arial" size="2">'.
	      (!empty($name)?'<p><font color="#888888">Name:</font><br />'.$name.'</p>':'').
				'<p>Some has commented on '.$title[1].'</p>'.
	      '<p><font color="#888888">Email:</font><br />'.$email.'</p>'.
	      '<p><font color="#888888">Comment:</font><br />'.$message.'</p>'.
				'<p><font color="#888888">(IP: '.$_SERVER['REMOTE_ADDR'].')</font></p>'.
				'<p><a href="'.strtolower($page->website).'a/newscomments">Click here to moderate this comment</a></p>'.
				'</font></body></html>';
			pun_mail(implode(',',$contact_emails),'[IAS] Comment for '.$title[1].' ('.date('d/m/Y H:i:s').')',$body,(!empty($name)?'"'.$name.'" ':'')."<{$email}>");
?>
    <div class="hr"></div>
    <p id="newcomment">Your comment has been added and is awaiting moderation.</p>
<?php 
     }
    }elseif(isset($_POST['addcomment'])){
?>
    <div class="hr"></div>
    <ul id="newcomment" class="error"><li>Possible hacking attempt detected. Comment has not been added.</li></ul>
<?php 
     $hideform=true;
		}
    $_SESSION['addcomment_code']=code(14).'-'.code(15).'-'.code(1);
		if(!isset($hideform)){
?>
    <div class="hr"></div>
    <h3>Add your own</h3>
    <form action="<?php echo $page->siteRoot;?>archives/<?php echo $filenum[1];?>#newcomment" method="post" class="post">
     <input type="hidden" name="addcomment" value="<?php echo $_SESSION['addcomment_code'];?>" />
     Name:<br />
     <input type="text" name="name" class="text" value="<?php echo htmlspecialchars($name);?>" /><br />
     Email (not shown):<br />
     <input type="text" name="email" class="text" value="<?php echo htmlspecialchars($email);?>" /><br />
     Comment:<br />
     <textarea name="message" class="text" cols="40" rows="5" style="height:60px;"><?php echo htmlspecialchars($message);?></textarea><br />
     <p class="bbcode">BBCode - You can use these tags: [b], [u], [i], [s], [url]</p>
     <p class="center"><input type="submit" name="submit" value="Add comment" class="button" /></p>
    </form>
<?php
		}
   }else{
?>
    <div class="hr"></div>
    <ul class="error"><li>Commenting for this post is now closed.</li></ul>
<?php 
   }
  }
 }else{
?>
    <h3>File cannot be found</h3>
    <ul class="error"><li>Sorry but the file you want to view just isn't there.</li></ul>
<?php 
 } 
}else{
 $page->head();

 if(isset($newsfiles)){
  foreach($newsfiles as $file){
   if($contents=@file_get_contents(ROOT.'data/news/'.$file)){
    preg_match('/<!--title\[(.*?)\]-->/',$contents,$title);
    preg_match('/<!--datetime\[(.*?)\]-->/',$contents,$date);
    preg_match('/<!--author\[(.*?)\]-->/',$contents,$author);
    preg_match('/<!--filenum\[(.*?)\]-->/',$contents,$filenum);
    preg_match('/<!--entry_text-->(.*?)<!--end_entry_text-->/s',$contents,$text);
    preg_match('/<!--comments_on\[(.*?)\]-->/',$contents,$comments_on);
    preg_match_all('/<!--comment_start--><!--comment_allowed\[1\]-->(.*?)<!--comment_end-->/s',$contents,$comments);
?>
    <h3 class="news"><a href="<?php echo $page->siteRoot;?>archives/<?php echo str_replace('.php','',$file);?>"><?php echo $title[1];?></a></h3>
    <p class="news">
     <span class="newsdate">Posted <?php echo date('F jS, Y \a\t H:ia',$date[1]);?></span>
     <span class="newscomments"><?php $nc=count($comments[1]);echo ($comments_on[1]=='1'?'<a href="'.$page->siteRoot.'archives/'.$filenum[1].'#comments">'.($nc>0?$nc:'No').' comment'.($nc==1?'':'s').'</a>':'Comments are closed');?> - by <?php echo $author[1];?></span>
     <br style="clear:both;" />
    </p>
    <?php echo bbcode(word_limit($text[1]),' class="newstext"');?> 
<?php if(str_word_count($text[1])>100){?>
    <p class="continue"><a href="<?php echo $page->siteRoot.'archives/'.$filenum[1];?>">Continue reading</a></p>
<?php }
	    if($session->is_admin()){?>
    <p style="margin-top:10px;"><a href="<?php echo $page->siteRoot.'a/editnews/'.$filenum[1];?>" class="edit">Edit item</a></p>
<?php }?>
    <div class="hr"></div>
<?php 
   }
  }
 }else{
?>
    <h3>No archived files found</h3>
    <ul class="error"><li>Sorry but it appears that there are no news entries, oh well!</li></ul>
<?php 
 }
}
$page->foot();
?>