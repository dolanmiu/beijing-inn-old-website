<?php  
/*
 * @project:  CGCU Website
 * @author:   Michael Haynes
 * @contact:  mjh105@imperial.ac.uk              
 * @date:     May 2008
 *
 * Please do not change anything 
 * unless you know what you're doing!
 *
*/ 
define('BEGIN',true);
define('ROOT','./');
include(ROOT.'include/common.php');

$page->title='Clubs &amp; Societies';
$page->pagetitle='Clubs &amp; Societies';

$deps=array_keys($data_clubs['deps']);
$deps=array_map('strtolower',$deps);
if(isset($_GET['c'])){
 $_club=strtolower(trim($_GET['c']));
 if(!in_array($_club,array_merge($deps,array('media','motor')))){
  header('Location: '.$page->siteRoot.'clubs-and-societies');
	exit;
 }
}else
 $_club=$deps[0];

$page->styles=array('div#content{',' width:760px;','}','div#content div#crufts{',' margin-right:0;','}');
$page->scripts=array("var club='{$_club}';",'$(document).ready(function(){',
 " $('.club').not('#d_'+club).hide();",
 " sIFR.replace(myriadpro,{selector:'#d_'+club+' p.dep',","  css:[","  '.sIFR-root{color:#810303;background-color:#ffffff}',","   'a{text-decoration:none;}',","   'a:link{color:#810303;}',","   'a:hover{color:#000000;}'","  ],","  wmode:'transparent'"," });",
 '});','function toggle_club(c){',
 " $('#d_'+club).hide();"," $('.anchors .'+club+'>a').removeClass('on');",
 " $('#d_'+c).show();"," $('.anchors .'+c+'>a').addClass('on');",
 " sIFR.replace(myriadpro,{selector:'#d_'+c+' p.dep',","  css:[","  '.sIFR-root{color:#810303;background-color:#ffffff}',","   'a{text-decoration:none;}',","   'a:link{color:#810303;}',","   'a:hover{color:#000000;}'","  ],","  wmode:'transparent'"," });",
 ' club=c;','}');
 
$page->head(true);
?>
    <ul id="upper_anchors" class="anchors"><?php 
foreach(array_slice($data_clubs['deps'],0,5) as $name=>$club){
 ?><li class="<?php echo strtolower($name);?>"><a href="<?php echo $page->siteRoot.'clubs-and-societies/'.strtolower($name);?>" onclick="toggle_club('<?php echo strtolower($name);?>');return false;"<?php echo (strtolower($name)==$_club?' class="on"':'');?>><?php echo $name;?></a></li><?php 
}
?></ul>
<?php 
foreach($data_clubs['deps'] as $name=>$club){
?>
    <div id="d_<?php echo strtolower($name);?>" class="club">
     <div>
      <p class="dep"><?php echo (isset($club['dep'])&&!empty($club['dep']))?$club['dep']:'Unknown Department';?></p>
      <div class="img"><a href="<?php echo $club['site'];?>"><img src="<?php echo $page->siteRoot.'image/?size=m200&amp;file=clubs/'.$club['img'].'&amp;bg_image=club_img_bg.png';?>" alt="Go to  <?php echo $name;?>'s website" title="Go to  <?php echo $name;?>'s website" /></a></div>
      <div class="info"><?php echo (isset($club['info'])&&!empty($club['info'])?bbcode($club['info']):'There currently isn\'t any club information!');?><p class="chair">- <?php echo (isset($club['chair'])&&!empty($club['chair']))?$club['chair']:'Unknown';?> <span>(Chair)</span></p></div>
      <p class="site">&#187; <a href="<?php echo $club['site'];?>"><?php echo $club['site'];?></a></p>
      <br style="clear:both;" />
     </div>
     <br style="clear:both;" />
    </div>
<?php 
}
?>
    <div id="d_motor" class="club">
     <div>
      <p>The [Motor Clubs] cater for a wide range of different motoring tastes, from [building a racing car|IC Racing] to looking after our [100 year old mascot, Boanerges|Boanerges].</p>
     </div>
     <br style="clear:both;" />
    </div>
    <div id="d_media" class="club">
     <div>
      <p>The [Media Group] controls all of Guilds media output: the freshers handbook Spanner, the engineering magazine Guildsheet and the jewel in the crown, [Live!|http://live.cgcu.net], the Imperial news website and general gossip monger.</p>
     </div>
     <br style="clear:both;" />
    </div>
    <ul id="lower_anchors" class="anchors"><?php 
foreach(array_merge(array_slice($data_clubs['deps'],5),array('Motor'=>'','Media'=>'')) as $name=>$club){
 ?><li class="<?php echo strtolower($name);?>"><a href="<?php echo $page->siteRoot.'clubs-and-societies/'.strtolower($name);?>" onclick="toggle_club('<?php echo strtolower($name);?>');return false;"<?php echo (strtolower($name)==$_club?' class="on"':'');?>><?php echo $name;?></a></li><?php 
}
?></ul>
    <br style="clear:both;" />
<?php 
$page->foot(false);
?>